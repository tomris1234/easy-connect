import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../_services/index';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Component({ templateUrl: 'home.component.html' })

export class HomeComponent implements OnInit {
  currentUser: any;
  userMessage = '';
  constructor(
    private authenticationService: AuthenticationService,
    private http: HttpClient
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
  }

  listAll: any = [];
  currentlistIndex = 0;
  apiUrl = environment.apiUrl;
  currentDocument;

  ngOnInit(): void {
    this.listallnotverified();
  }

  listallnotverified(): void {
    const headers = { token: localStorage.getItem('token'), 'Content-Type': 'application/json' };
    this.http.get(`${environment.apiUrl}api/v1/users/list/allnotverified`, { headers }).subscribe(
      data => {
        const d: any = data;
        this.listAll = d.data;
        this.loadDocument(d.data[0]);
      },
      error => {
        console.error(error);
      });
  }

  // tslint:disable-next-line: typedef
  loadDocument(user) {
    if (user) {
      const headers = { token: localStorage.getItem('token'), 'Content-Type': 'application/json' };
      this.http.get(`${environment.apiUrl}api/v1/verifications/oneByUser/${user.id}`, { headers }).subscribe(data => {
        let d: any = data;
        this.currentDocument = d.data[0];
        console.error(d.data[0]);
      });
    } else {
      this.userMessage = 'Users Not Found'
    }
  }
  // tslint:disable-next-line: typedef
  getCurrentDocument(i, user) {
    this.currentlistIndex = i;
    this.loadDocument(user);
  }

  // tslint:disable-next-line: typedef
  updateStatus(userId, status) {
    const headers = { token: localStorage.getItem('token'), 'Content-Type': 'application/json' };
    const body = {
      userId,
      status
    };

    this.http.post(`${environment.apiUrl}api/v1/verifications/veriy`, body, { headers }).subscribe(data => {
      let d: any = data;
      if (d.success) {
        alert(d.message);
        // tslint:disable-next-line: whitespace
        // this.listAll = this.listAll.splice(this.currentlistIndex, 1);
        console.error(this.currentlistIndex);
        this.listAll = this.listAll.filter(function (value, index, arr) {
          return value._id !== userId;
        });
        this.loadDocument(this.listAll[0]);
        this.currentlistIndex = 0;
      }
    });
  }
}

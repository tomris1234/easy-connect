import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {environment} from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;
    private apiUrl = 'http://localhost:7777/';
    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): any {
        return this.currentUserSubject.value;
    }

    // tslint:disable-next-line: typedef
    login(username, password) {
        return this.http.post<any>(`${environment.apiUrl}api/v1/users/admin/login`, { username, password })
            .pipe(map(user => {
                console.error(user);
                localStorage.setItem('currentUser', JSON.stringify(user.data.data));
                localStorage.setItem('token', user.data.token);
                this.currentUserSubject.next(user);
                return user.data.data;
            }));
    }


    // tslint:disable-next-line: typedef
    getAll() {
        const headers = { token:  localStorage.getItem('token'), 'Content-Type' : 'application/json' };
        return this.http.get(`${environment.apiUrl}api/v1/users/list/allnotverified` , {headers} )
            .pipe(map(user => {
                return user;
            }));
    }
    // tslint:disable-next-line: typedef
    logout() {
        localStorage.removeItem('currentUser');
        localStorage.removeItem('token');
        this.currentUserSubject.next(null);
    }
}

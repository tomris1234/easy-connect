# Easy-Connect
Connecting people with disabilities with university students




# Description 
What is the problem?
In many cases people with disabilities in times of pandemic cannot reach out to their basic needs and services, feel neglected and left behind. In addition, there are significant numbers of university students who are not only in loan debt but also are experiencing financial hardships. 

Our solution:
We decided to connect these two groups of people where students would be able to serve people with disabilities so that they could earn some money from relevant services. 
 
Completeness:
We used Heroku server to host Node JS code, mongo Db and Watson Assistant. We completed messaging where student and disabled person or admin are going to communicate, sign in/ up page, and Gmail notification to mark the completion of the service by people with disabilities and students. 

Transferability:
This system can also be used for selling general services such as house repairment, domestic chores, beauty services, doctor appointment and other needs. 

Effectiveness: 
As it was mentioned above, the aim of our app is to connect people with disabilities and students. In our app disabled person by posting a request page about the details of the service and expected payment is made to potential service provider - student. 

Efficiency: 
For our app to work well we need to make sure that there are enough service providers for people with disabilities and vice versa. We will distribute the information of the app among the NGOs and other relevant institutions working for people with disabilities for them to have an access of getting services. The same is true for promoting this project among universities and colleges. 

Design: 
To make sure that there are no fake users, admin is going to review students and people with disabilities by asking them some questions like university they are enrolled in, their matriculation number, disability card number, picture of disability card, so on. 
Making sure that service provider or disabled person is from the targeted community and no one is taking unfair advantage from the app for abusing it. After being verified student or disabled person is free to use the app. Disabled person places a request for service and student accepts it. 
To signal the completion of the service, student is going to notify admin. In this case notification is going to be sent to disabled person via email to confirm that the student has completed the service.  

Usability: 
In our opinion the challenging part of the app is the verification of identity where admin is screening students and people with disabilities by asking them few questions. The other parts of the app are easy to use.

Creativity and Innovation
There are various virtual platforms for people with disabilities to reach out to their support worker from hospitals or other care facilities. There are also some programs where university students can voluntarily help to people with disabilities. However, in the practice, there is no such program where students and people with disabilities mutually benefit from each other. It mainly constitutes the uniqueness of this App.  




https://www.hrw.org/news/2020/03/26/protect-rights-people-disabilities-during-covid-19
https://news.un.org/en/story/2020/03/1059762
https://www.businessinsider.com/student-loan-debt-crisis-college-cost-mind-blowing-facts-2019-7#11-nearly-50-of-millennials-who-have-or-had-student-loan-debt-think-college-wasnt-worthwhile-11 



# The Architecture 
![flow_chart](/uploads/e365799886888c7c0fa2a4e0479885cd/flow_chart.jpg)



1.	User (person with disabilities/student) navigates through the mobile app.
2.	Admin verifies user (person with disabilities/student) through backend – Angular.
https://drive.google.com/file/d/11ndvSvdRpqLZbpO2K9tFLwqk0zeLyqQh/view?usp=sharing
3.	mongo DB provides a database such as availability of services for people with disabilities, location of the recipient, duration, expenses, etc. 
4.	User (person with disabilities/student) can have simple communication with other users (person with disabilities/student) with Watson Assistant.
5.  User (person with disabilities/student) can communicate with other users (person with disabilities/student) with real time messaging provided by Firebase
6.	Heroku hosts Node JS code. 

# Demo Video
https://drive.google.com/file/d/1jxEdvmVgcuN7H-xNk2Pg_BJ1JtpfABxP/view?usp=sharing


# Solution Map
![Solution map EasyConnect](https://user-images.githubusercontent.com/68922271/88844784-477fba80-d1f4-11ea-9af3-7e3bc2e1112f.jpg)
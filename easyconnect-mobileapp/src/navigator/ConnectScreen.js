import React from 'react';

import {createDrawerNavigator} from '@react-navigation/drawer';

import { DrawerContent } from '../screens/DrawerContent';
import HistoryScreen from '../screens/HistoryScreen';
import MyProfileScreen from '../screens/MyProfileScreen';
import BidRequestScreen from '../screens/BidRequestScreen';
import HomeStackScreen from './HomeStackScreen';

const Drawer = createDrawerNavigator();

const ConnectScreen = ({navigation}) => (
        <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}
          drawerStyle={{
            backgroundColor: '#cad0d4',
          }}>
          <Drawer.Screen name="HomeStackScreen" component={HomeStackScreen} headerMode='none' />
          <Drawer.Screen name="BidRequestScreen" component={BidRequestScreen} />
          <Drawer.Screen name="HistoryScreen" component={HistoryScreen} />
          <Drawer.Screen name="MyProfileScreen" component={MyProfileScreen} headerMode='none'/>
        </Drawer.Navigator>
);

export default ConnectScreen;
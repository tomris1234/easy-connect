import React from 'react';

import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { useSelector, useDispatch } from 'react-redux'

import Icon from 'react-native-vector-icons/Ionicons';

import HomeScreen from '../screens/HomeScreen';
import MessageScreen from '../screens/MessageScreen';
import NotificatonsScreen from '../screens/NotificationsScreen';
import ProfileScreen from '../screens/ProfileScreen';

const HomeStack = createStackNavigator();
const DetailsStack = createStackNavigator();

const Tab = createMaterialBottomTabNavigator();

const MainTabScreen = () => (
    <Tab.Navigator
      initialRouteName="Home"
      activeColor="#fff"
      labeled= 'true'
      barStyle={{
        backgroundColor: '#009387'
      }}
    >
      <Tab.Screen
        name="Home"
        component={HomeStackScreens}
        options={{
          tabBarLabel: 'Home',
          tabBarColor: '#009387',
          tabBarIcon: ({ color }) => (
            <Icon name="ios-home" color={color} size={26} />
          ),
        }}
      />

      <Tab.Screen
        name="Message"
        component={MessageStackScreen}
        options={{
          tabBarLabel: 'Messages',
          tabBarColor: '#009387',
          tabBarIcon: ({ color }) => (
            <Icon name="ios-chatbubbles" color={color} size={26} />
          ),
        }}
      />

      <Tab.Screen
        name="Profile"
        component={ProfileStackScreen}
        options={{
          tabBarLabel: 'Profiles',
          tabBarColor: '#009387',
          tabBarIcon: ({ color }) => (
            <Icon name="md-people" color={color} size={26} />
          ),
        }}
      />
      {/* <Tab.Screen
        name="Notifications"
        component={NotificatonsStackScreen}
        options={{
          tabBarLabel: 'Notifications',
          tabBarColor: '#009387',
          tabBarIcon: ({ color }) => (
            <Icon name="ios-notifications" color={color} size={26} />
          ),
        }}
      /> */}
    </Tab.Navigator>
);

export default MainTabScreen;

const HomeStackScreens = ({navigation}) => (
    <HomeStack.Navigator screenOptions={{
            headerStyle: {
            backgroundColor: '#009387',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
            fontWeight: 'bold'
            }
        }}>
            <HomeStack.Screen name="HomeStack" component={HomeScreen} options={{
            title:'Home',
            headerLeft: () => (
                <Icon.Button name="ios-menu" size={30} backgroundColor="#009387" style={{marginLeft: 8}} onPress={() => navigation.openDrawer()}></Icon.Button>
            )
            }} />
    </HomeStack.Navigator>
    );
    
    const MessageStackScreen = ({navigation}) => (
    <DetailsStack.Navigator screenOptions={{
            headerStyle: {
            backgroundColor: '#009387',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
            fontWeight: 'bold'
            }
        }}>
            <DetailsStack.Screen name="MessageStack" component={MessageScreen} options={{
            title:'Messages',
            headerLeft: () => (
                <Icon.Button name="ios-menu" size={30} backgroundColor="#009387" style={{marginLeft: 8}} onPress={() => navigation.openDrawer()}></Icon.Button>
            )
            }} />
    </DetailsStack.Navigator>
    );

    const ProfileStackScreen = ({navigation}) => (
        <HomeStack.Navigator screenOptions={{
                headerStyle: {
                backgroundColor: '#009387',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                fontWeight: 'bold'
                }
            }}>
                <HomeStack.Screen name="ProfileStack" component={ProfileScreen} options={{
                title:'Profiles',
                headerLeft: () => (
                    <Icon.Button name="ios-menu" size={30} backgroundColor="#009387" style={{marginLeft: 8}} onPress={() => navigation.openDrawer()}></Icon.Button>
                )
                }} />
        </HomeStack.Navigator>
        );
        
        // const NotificatonsStackScreen = ({navigation}) => (
        // <DetailsStack.Navigator screenOptions={{
        //         headerStyle: {
        //         backgroundColor: '#009387',
        //         },
        //         headerTintColor: '#fff',
        //         headerTitleStyle: {
        //         fontWeight: 'bold'
        //         }
        //     }}>
        //         <DetailsStack.Screen name="NotificationsStack" component={NotificatonsScreen} options={{
        //         title:'Notificatons',
        //         headerLeft: () => (
        //             <Icon.Button name="ios-menu" size={30} style={{marginLeft: 8}} backgroundColor="#009387" onPress={() => navigation.openDrawer()}></Icon.Button>
        //         )
        //         }} />
        // </DetailsStack.Navigator>
        // );
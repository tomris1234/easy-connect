import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';

import {createStackNavigator} from '@react-navigation/stack';
import MainTabScreen from './MainTabScreen';
import JobsDetails from '../screens/JobsDetails';
import PostJobScreen from '../screens/PostJobScreen';
import ChatScreen from '../screens/ChatScreen';
import BidedDetailsScreen from '../screens/BidedDetailsScreen';
import OthersProfileScreen from '../screens/OthersProfileScreen';

const HomeStack = createStackNavigator();

const HomeStackScreen =({navigation})=> {
  return (
      <HomeStack.Navigator >
          <HomeStack.Screen name="HomeDrawer" component={MainTabScreen} options={{headerShown:false}}/>
          <HomeStack.Screen name="JobsDetails" component={JobsDetails} options={{
            title:'Job Details',
            headerStyle: { backgroundColor: '#009387' },
            headerTitleStyle: { color: '#fff' },
            headerLeft: () => (
                <Icon.Button name="ios-arrow-back" size={30} backgroundColor="#009387" style={{marginLeft: 8}} onPress={() => navigation.navigate('HomeStack')}></Icon.Button>
            )
            }} />
          <HomeStack.Screen name="PostJobScreen" component={PostJobScreen} options={{
            title:'Post Job',
            headerStyle: { backgroundColor: '#009387' },
            headerTitleStyle: { color: '#fff' },
            headerLeft: () => (
                <Icon.Button name="ios-arrow-back" size={30} backgroundColor="#009387" style={{marginLeft: 8}} onPress={() => navigation.navigate('HomeStack')}></Icon.Button>
            )
            }} />
            <HomeStack.Screen name="ChatScreen" component={ChatScreen} options={{
            title:'Chat',
            headerStyle: { backgroundColor: '#009387' },
            headerTitleStyle: { color: '#fff' },
            headerLeft: () => (
                <Icon.Button name="ios-arrow-back" size={30} backgroundColor="#009387" style={{marginLeft: 8}} onPress={() => navigation.goBack()}></Icon.Button>
            )
            }} />
            <HomeStack.Screen name="BidedDetailsScreen" component={BidedDetailsScreen} options={{headerShown:false}} />
            <HomeStack.Screen name="OthersProfileScreen" component={OthersProfileScreen} options={{headerShown:false}}/>
      </HomeStack.Navigator>
  );
}

export default HomeStackScreen;
import React, {useEffect, useLayoutEffect, useState} from 'react';
import {ScrollView, Text, Button, StyleSheet, ActivityIndicator} from 'react-native';
import Message from '../components/Message';
import contact from '../../assets/contact.png';
import {useSelector} from "react-redux";
import firebaseSDK from "../firebase/firebaseSdk";

const MessageScreen = ({navigation}) => {
    const userId = useSelector(state => state.loginReducers.data);

    const [chatRooms, setChatrooms]= useState();
    const [messages, setMessages]= useState();

    useEffect(() => {
        console.log('getting messages');
        let chatRooms = [];

        let chatUserRef = firebaseSDK.ChatUserRef(userId.data._id);
        chatUserRef.on("value", snap => {
            snap.forEach(child => {
                chatRooms.push({...child.val()});
            });

        chatRooms.forEach((chatRoom)=>{
            const chatRef = firebaseSDK.ChatRef(chatRoom.chat_id).limitToLast(1);
            chatRef.on("value", snap => {
                let messages = [];
                snap.forEach(child => {
                    messages.push({...child.val()})
                })
                setMessages(messages)
            });
        })
        })
    },[]);



    return (
      <ScrollView contentContainerStyle={styles.container}>
          {messages && messages.map((item,index) =>
              <Message key = {index} title={item.text} details= {'By: '+item.user.name} icon={contact} messageDetail={item} currentUserId={userId.data._id}/>

          )}

      </ScrollView>
    );
};

export default MessageScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    alignItems: 'center', 
    // justifyContent: 'center'
  },
});
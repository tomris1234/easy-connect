import React, {useState, useEffect} from 'react';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import { View, Text, Keyboard, Alert, StyleSheet, TouchableWithoutFeedback, TextInput, SafeAreaView, FlatList, Image, ActivityIndicator, StatusBar } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'

import JobPost from '../components/JobPost';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Modal from 'react-native-modal';
// import { List } from "react-native-elements";

const HomeScreen = ({navigation}) => {
  const [open, setVerify] = useState(true);
  const [data, setData] = useState();
  const [loading, setLoading] = useState(false);
  const [loadingv, setLoadingv] = useState(false);
  const [isFetching, setFetch] = React.useState(false);
  const [photo, setImage] = React.useState({
    id: '',
    image: '',
    type: '',
    filename: ''
  });
  const loginReducers = useSelector(state => state.loginReducers.data)

  useEffect(() => {
    getPermissionAsync();
    makeRequest()
  }, []);

  const onRefresh = () => {
    setFetch(true);
    makeRequest();
  }

  const getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  };

  const _pickImage = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      });
      if (!result.cancelled) {
        let localUri = result.uri;
        let filename = localUri.split('/').pop();
        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`;

        setImage({
          ...photo,
          image: result.uri,
          filename: filename,
          type: type
        });
      }
    } catch (E) {
      console.log(E);
    }
  };

  const makeVerify = () => {
    let formData = new FormData();
    formData.append('Id',photo.id);
    formData.append('documentType',loginReducers.data.role); 
    formData.append('fileUrl', {type:photo.type, uri:photo.image, name:photo.filename});

    const url = `https://ibm-challenge-api.herokuapp.com/api/v1/verifications`;
    setLoadingv(true)
    fetch(url,
      {
        method: 'POST',
        headers: {  // these could be different for your API call
          'Content-Type': 'application/json',
          'token': loginReducers.token
        },
        body: formData
      })
      .then((response) => response.json())
      .then((json) => {
        // console.log(json, 'allll jobs');
        if (json.success === true) { // response success checking logic could differ
          Alert.alert('Success', 'Successfully sent for verification', [
            {text: 'Ok'}
          ])
          setLoadingv(false)
        }
        else{
          Alert.alert('Error', 'Some field is missing', [
            {text: 'Ok'}
          ])
          setLoadingv(false)
        }
      })
      .catch((err) => {
        console.log(err);
        setLoadingv(false)
      });
  };


  const makeRequest = () => {
    const url = `https://ibm-challenge-api.herokuapp.com/api/v1/jobs/all`;
    setLoading(true)
    fetch(url,
      {
        method: 'GET',
        headers: {  // these could be different for your API call
          'Content-Type': 'application/json',
          'token': loginReducers.token
        },
      })
      .then((response) => response.json())
      .then((json) => {
        // console.log(json, 'allll jobs');
        if (json.success === true) { // response success checking logic could differ
          setData(json.data.reverse())
          setLoading(false)
          setFetch(false);
        // console.log(json.data, 'allll job dtas listttttttttttttttttt');
        }
        setFetch(false);
        setLoading(false)
      })
      .catch((err) => {
        console.log(err);
        setLoading(false)
        setFetch(false);
      });
  };


  const openModal = () => {
    setVerify(!open);
  }

  const showModal = () => {
    return(
      <Modal 
      animationIn="slideInUp"
      isVisible={open}
      onBackdropPress={()=> openModal()}
      onBackButtonPress={()=> openModal()}
      style={{flex:1, justifyContent: 'flex-end', alignItems: 'center', }}
      >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <View style={{height: '90%', width: '100%', backgroundColor: '#fff'}}>
              <Text style={styles.bidding}> Verify your account </Text>
              <View style={styles.Rate}>
                <Text style={styles.rateTitle}> Your ID Number: </Text>
                <TextInput 
                  placeholder="Expected Rate"
                  placeholderTextColor="#9E9E9E"
                  style={styles.textInput}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                />
              </View>
              <View style={styles.image}>
                <View style={styles.imageBox}>
                  <Image source={{ uri: photo.image }} style={{ flex: 1 }} />
                </View>
                <TouchableOpacity style={styles.upload} onPress={()=> _pickImage()}>
                  <Text style={{color: '#fff'}}>Upload ID</Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity style={styles.bidSubmit} onPress={()=> makeVerify()}>
                  <Text style={styles.bidText}> Verify </Text>
              </TouchableOpacity>
              {loadingv ? <ActivityIndicator size="small" color="#009387" /> : null}
              <View style={styles.note}>
                <Text><Text style={{color: 'red', paddingTop: 8}}>*</Text> You must verify your account </Text>
              </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }


    return (
      <View style={styles.container}>
        <StatusBar backgroundColor='#009387' barStyle="light-content"/>
        <SafeAreaView style={{flex:1}}>
          {loading ? <ActivityIndicator size="small" color="#009387" style={{paddingTop: 6}}/> : null}
          <FlatList
            data={data}
            refreshing={isFetching}
            onRefresh={() => onRefresh()}
            renderItem={({ item }) => (
              <JobPost
                item={item}
                key={item.id}
              />
            )}
          />
        </SafeAreaView >

        {
        loginReducers.data.role === 'Guest' ?
          <View style={{position: 'absolute', bottom: 10, alignSelf: 'center'}}>
            {(loginReducers.data.isVerified === 'Verified') ? 
            <TouchableOpacity style={styles.post} onPress={()=> navigation.navigate('PostJobScreen')}>
              <Text style={styles.postButton}> Post Jobs </Text>
            </TouchableOpacity>
            :
            <TouchableOpacity style={styles.postNo} onPress={()=> 
              Alert.alert('No Access!', 'Your Account Must be Verified', [
                {text: 'Ok'}
            ])
            }>
              <Text style={styles.postButton}> Post Jobs </Text>
            </TouchableOpacity>
            }
          </View>
        : 
        null
        }
        {loginReducers.data.isVerified === 'Verified' ? null : showModal()}
      </View>
    );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
  },

  post: {
    alignSelf: 'center',
    height: 40,
    width: 120,
    borderRadius: 20,
    backgroundColor: '#009387',
    justifyContent: 'center',
    alignItems: 'center',
  },

  postNo: {
    marginTop: 5,
    alignSelf: 'center',
    height: 40,
    width: 120,
    borderRadius: 20,
    backgroundColor: '#777',
    justifyContent: 'center',
    alignItems: 'center',
  },

  postButton: {
    color: 'white',
    alignSelf: 'center',
  },

  //verify
  bidding: {
    color: '#009387',
    fontSize: 18,
    fontWeight: 'bold',
    padding: 8
  },

  Rate: {
    minHeight: 72,
  },

  rateTitle: {
    paddingLeft: 8,
    paddingTop: 8,
    paddingBottom: 5
  },

  textInput: {
    height: 45,
    width: '93%',
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
    borderWidth: 1,
    alignSelf: 'center',
    borderColor: '#009387',
    borderRadius: 10,
    backgroundColor: '#eff0f1'
  },

  image: {
    marginTop: 8,
    height: 260,
    width: '100%'
  },

  imageBox: {
    height: 220,
    width: '93%',
    color: '#05375a',
    borderWidth: 1,
    alignSelf: 'center',
    borderColor: '#009387',
    borderRadius: 10,
    backgroundColor: '#eff0f1'
  },

  upload: {
    marginLeft: 12,
    marginTop: 8,
    height: 28,
    width: 90,
    borderRadius: 8,
    backgroundColor: '#a09c9c',
    justifyContent: 'center',
    alignItems: 'center'
  },

  bidSubmit: {
    margin: 8,
    alignSelf: 'center',
    height: 40,
    width: 120,
    borderRadius: 20,
    backgroundColor: '#009387',
    justifyContent: 'center',
    alignItems: 'center',
  },

  note: {
    height: 40,
    position: 'absolute',
    alignSelf: 'center',
    justifyContent :'center',
    bottom: 0
  },

  bidText: {
    color: 'white',
    alignSelf: 'center',
  },
});            
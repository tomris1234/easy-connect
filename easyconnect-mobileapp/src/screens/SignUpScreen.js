import React from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    ActivityIndicator,
    TextInput,
    Platform,
    StyleSheet,
    ScrollView,
    StatusBar,
    Alert
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Button, Menu, Divider, Provider } from 'react-native-paper';
import Feather from 'react-native-vector-icons/Feather';
import { register } from '../redux/register/actions';
import { useSelector, useDispatch } from 'react-redux'

const SignUpScreen = ({navigation}) => {
    const [visible, setVisible] = React.useState(true);
    const [data, setData] = React.useState({
        username: '',
        role: 'Student',
        password: '',
        phone: '',
        fullname: '',
        lastname: ' ',
        check_textInputChange: false,
        check_nameInputChange: false,
        check_phoneInputChange: false,
        secureTextEntry: true,
    });

    const dispatch = useDispatch();
    const registerReducers = useSelector(state => state.registerReducers)

    React.useEffect(() => {
        console.log(registerReducers, 'api dataaaaaaaaaaaaaa');
        return error()
      }, [registerReducers] )

    const openMenu = () => setVisible(true);
    const closeMenu = () => setVisible(false);

    const error = () => {
        if(registerReducers.error !== ''){
            Alert.alert(registerReducers.error, registerReducers.errormessage, [
                {text: 'Okay'}
            ])
        }
        if(registerReducers.isRegister){
            Alert.alert('Success', registerReducers.data.message, [
                {text: 'Okay'}
            ])
            navigation.navigate('SignInScreen')
        }
    }

    const textInputChange = (val) => {
        setData({
            ...data,
            username: val,
            check_textInputChange: true
        });
    }

    const nameInputChange = (val) => {
        setData({
            ...data,
            fullname: val,
            check_nameInputChange: true
        });
    }

    const phoneInputChange = (val) => {
        setData({
            ...data,
            phone: val,
            check_phoneInputChange: true
        });
    }

    const textLoginChange = (val) => {
            setData({
                ...data,
                role: val,
            });
    }

    const handlePasswordChange = (val) => {
        setData({
            ...data,
            password: val
        });
    }

    const updateSecureTextEntry = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        });
    }

    const registerHandle = (data) => {
        console.log(data, 'sssssssssssgnup data');
        dispatch(register(data))
    }

    return (
        <Provider>
      <View style={styles.container}>
          <StatusBar backgroundColor='#009387' barStyle="light-content"/>
        <View style={styles.header}>
            <Text style={styles.text_header}>Register Now!</Text>
        </View>
        <Animatable.View 
            animation="fadeInUpBig"
            style={styles.footer}
        >
            <ScrollView showsVerticalScrollIndicator={false} style={{zIndex: 0}}>
            <Text style={styles.text_footer}>Email or Username</Text>
            <View style={styles.action}>
                <FontAwesome 
                    name="user-o"
                    color="#05375a"
                    size={20}
                />
                <TextInput 
                    placeholder="Your Username"
                    style={styles.textInput}
                    autoCapitalize="none"
                    onChangeText={(val) => textInputChange(val)}
                />
                {data.check_textInputChange ? 
                <Animatable.View
                    animation="bounceIn"
                >
                    <Feather 
                        name="check-circle"
                        color="green"
                        size={20}
                    />
                </Animatable.View>
                : null}
            </View>

            <Text style={[styles.text_footer, {
                marginTop: 35}]}>Fullname</Text>
            <View style={styles.action}>
                <FontAwesome 
                    name="user-o"
                    color="#05375a"
                    size={20}
                />
                <TextInput 
                    placeholder="Fullname"
                    style={styles.textInput}
                    autoCapitalize="none"
                    onChangeText={(val) => nameInputChange(val)}
                />
                {data.check_nameInputChange ? 
                <Animatable.View
                    animation="bounceIn"
                >
                    <Feather 
                        name="check-circle"
                        color="green"
                        size={20}
                    />
                </Animatable.View>
                : null}
            </View>

            <Text style={[styles.text_footer,  {
                marginTop: 35}]}>Phone Number</Text>
            <View style={styles.action}>
                <FontAwesome 
                    name="mobile"
                    color="#05375a"
                    size={28}
                />
                <TextInput 
                    placeholder="Your Phone Number"
                    keyboardType= 'phone-pad'
                    style={styles.textInput}
                    autoCapitalize="none"
                    onChangeText={(val) => phoneInputChange(val)}
                />
                {data.check_phoneInputChange ? 
                <Animatable.View
                    animation="bounceIn"
                >
                    <Feather 
                        name="check-circle"
                        color="green"
                        size={20}
                    />
                </Animatable.View>
                : null}
            </View>

            <Text style={[styles.text_footer, {
                marginTop: 35
            }]}>Login as</Text>
            <View style={styles.actionDrop}>
                <Feather 
                    name="users"
                    color="#05375a"
                    size={20}
                    style= {{marginTop: 15}}
                />
                <View
                    style={{
                    width: '100%',
                    paddingTop: 15,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    }}>
                    <Text style={{paddingLeft: 12, fontSize: 14}}>{data.role}</Text>
                    <Menu
                    visible={visible}
                    stye={{width: 400}}
                    onDismiss={()=> closeMenu()}
                    anchor={<Button onPress={() => openMenu()}>
                    <FontAwesome 
                    name="chevron-down"
                    color="#009387"
                    size={20}
                />
                    </Button>}>
                    <Menu.Item onPress={() => textLoginChange('Student')} title="Student" />
                    <Menu.Item onPress={() => textLoginChange('Guest')} title="Guest" />
                    </Menu>
                </View>
            </View>

            <Text style={[styles.text_footer, {
                marginTop: 35
            }]}>Password</Text>
            <View style={styles.action}>
                <Feather 
                    name="lock"
                    color="#05375a"
                    size={20}
                />
                <TextInput 
                    placeholder="Your Password"
                    secureTextEntry={data.secureTextEntry ? true : false}
                    style={styles.textInput}
                    autoCapitalize="none"
                    onChangeText={(val) => handlePasswordChange(val)}
                />
                <TouchableOpacity
                    onPress={updateSecureTextEntry}
                >
                    {data.secureTextEntry ? 
                    <Feather 
                        name="eye-off"
                        color="grey"
                        size={20}
                    />
                    :
                    <Feather 
                        name="eye"
                        color="grey"
                        size={20}
                    />
                    }
                </TouchableOpacity>
            </View>

            <View style={styles.textPrivate}>
                <Text style={styles.color_textPrivate}>
                    By signing up you agree to our
                </Text>
                <Text style={[styles.color_textPrivate, {fontWeight: 'bold'}]}>{" "}Terms of service</Text>
                <Text style={styles.color_textPrivate}>{" "}and</Text>
                <Text style={[styles.color_textPrivate, {fontWeight: 'bold'}]}>{" "}Privacy policy</Text>
            </View>

            {registerReducers.isLoading ? <ActivityIndicator size="small" color="#009387" /> : null}

            <View style={styles.button}>
                <TouchableOpacity
                    style={styles.signUp}
                    onPress={() => {registerHandle(data)}}
                >
                <View
                    style={styles.signIn}
                >
                    <Text style={[styles.textSign, {
                        color:'#fff'
                    }]}>Sign Up</Text>
                </View>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => navigation.goBack()}
                    style={[styles.signIn, {
                        borderColor: '#009387',
                        borderWidth: 1,
                        marginTop: 15
                    }]}
                >
                    <Text style={[styles.textSign, {
                        color: '#009387'
                    }]}>Sign In</Text>
                </TouchableOpacity>
            </View>
            </ScrollView>
        </Animatable.View>
      </View>
      </Provider>

    );
};

export default SignUpScreen;

const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor: '#009387'
    },
    header: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 50
    },
    footer: {
        flex: Platform.OS === 'ios' ? 3 : 5,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 30
    },
    text_header: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 30
    },
    text_footer: {
        color: '#05375a',
        fontSize: 18
    },
    action: {
        // zIndex: 10,
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    actionDrop: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
    },
    button: {
        alignItems: 'center',
        marginTop: 50
    },
    signUp: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        backgroundColor: '#009387'
    },
    signIn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    textSign: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    textPrivate: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 20
    },
    color_textPrivate: {
        color: 'grey'
    }
  });
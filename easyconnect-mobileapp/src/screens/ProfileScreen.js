import React, {useState, useEffect} from 'react';
import { ActivityIndicator, SafeAreaView, FlatList, StyleSheet } from 'react-native';
import ProfileBox from '../components/ProfileBox';
import { useSelector, useDispatch } from 'react-redux'

const ProfileScreen = () => {
  const [data, setData] = useState();
  const [loading, setLoading] = useState(false);
  const [isFetching, setFetch] = useState(false);
  const loginReducers = useSelector(state => state.loginReducers.data)

  useEffect(() => {
    loginReducers.data.role === "Guest" ? 
    makeStudentRequest()
    : 
    makeGuestRequest()
    // console.log(data, 'data from api list')
  }, []);

  // const onRefresh = () => {
  //   setFetch(true);
  //   loginReducers.data.role === "Guest" ? makeStudentRequest() : makeGuestRequest()
  // }

  const makeGuestRequest = () => {
    const url = `https://ibm-challenge-api.herokuapp.com/api/v1/users/list/disable`;
    setLoading(true)
    fetch(url,
      {
        method: 'GET',
        headers: {  // these could be different for your API call
          'Content-Type': 'application/json',
          'token': loginReducers.token
        },
      })
      .then((response) => response.json())
      .then((json) => {
        // console.log(json);
        if (json.success === true) { // response success checking logic could differ
          setData(json.data)
          setLoading(false)
          setFetch(false);
        } else {
          setLoading(false)
          setFetch(false);
        }
      })
      .catch((err) => {
        console.log(err);
        setLoading(false)
        setFetch(false);
      });
  };

  const makeStudentRequest = () => {
    const url = `https://ibm-challenge-api.herokuapp.com/api/v1/users/list/students`;
    fetch(url,
      {
        method: 'GET',
        headers: {  // these could be different for your API call
          'Content-Type': 'application/json',
          'token': loginReducers.token
        },
      })
      .then((response) => response.json())
      .then((json) => {
        // console.log(json);
        if (json.success === true) { // response success checking logic could differ
          setData(json.data)
        } else {
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <SafeAreaView style={styles.container}>
        {loading ? <ActivityIndicator size="small" color="#009387" style={{paddingTop: 6}}/> : null}
        <FlatList
            data={data}
            // refreshing={isFetching}
            // onRefresh={() => onRefresh()}
            renderItem={({ item }) => (
              <ProfileBox
                username= {item.username}
                firstname= {item.firstname}
                lastname= {item.lastname}
                item={item}
                key= {item.key}
              />
            )}
          />
    </SafeAreaView>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    width: '100%',
  },
});
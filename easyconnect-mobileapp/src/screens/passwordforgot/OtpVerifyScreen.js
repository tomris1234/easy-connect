import React from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';

const OtpVerifyScreen = () => {
    return (
      <View style={styles.container}>
        <Text>Otp Verify Screen</Text>
        <Button
          title="Click Here"
          onPress={() => alert('Button Clicked!')}
        />
      </View>
    );
};

export default OtpVerifyScreen
;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
});
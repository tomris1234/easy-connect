import React from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';

const EmailOtpScreen = () => {
    return (
      <View style={styles.container}>
        <Text>Email Otp Screen</Text>
        <Button
          title="Click Here"
          onPress={() => alert('Button Clicked!')}
        />
      </View>
    );
};

export default EmailOtpScreen
;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
});
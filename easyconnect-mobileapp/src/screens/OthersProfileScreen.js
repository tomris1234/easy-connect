import React from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    TextInput,
    Image,
    StyleSheet ,
    StatusBar,
    ScrollView
} from 'react-native';
import {
  Avatar,
  Title,
  Caption,
} from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import Summary from '../components/Summary';
import Skills from '../components/Skills';
import profile from '../../assets/person.png';

const OthersProfileScreen = ({navigation, route}) => {
  const { firstname } = route.params.item;
  const { lastname } = route.params.item;
  const { username } = route.params.item;
  const { phone } = route.params.item;
  const { role } = route.params.item;
  const { isVerified } = route.params.item;
    return (
      <ScrollView contentContainerStyle={styles.container}>
          <StatusBar backgroundColor='#009387' barStyle="light-content"/>
        <View style={styles.header}>
            <View style={styles.titleBar}>
              <TouchableOpacity onPress={()=> navigation.goBack()}>
                <Icon name="ios-arrow-back" color='#dcdcdc' size={30} />
              </TouchableOpacity>
              <Text style={{color: '#fff', paddingTop: 6, fontSize: 18}}>Profile</Text>
              <Icon name="md-more" color='#009387' size={32} />
            </View>
            <View style={{width: '100%', borderWidth: 0.4, borderColor: '#fff', marginHorizontal: 0}} />
            <View style={{flexDirection:'column',marginTop: 20,}}>
                <View style={{height:150, width: 150, borderRadius: 70, alignSelf: 'center'}}>
                  <Image source={profile} style={styles.pic}/>
                </View>
                <View style={{marginLeft:15, flexDirection:'column'}}>
                  <Title style={styles.title}>{firstname} {lastname}</Title>
                  <Caption style={styles.caption}>{username}</Caption>
                </View>
            </View>
        </View>
        <View style={styles.footer}>
          <View style={styles.footerinside}>
            {/* <Summary from={false}/>
            <Skills from={false}/> */}
            <View style= {{height: 50, justifyContent: 'center'}}>
              <Caption style={styles.caption2}>
                <Text style={{fontSize: 17, color: '#009387'}}>Phone:</Text>
                {phone}
              </Caption>
            </View>
            <View style= {{height: 40, justifyContent: 'center'}}>
              <Caption style={styles.caption2}>
                <Text style={{fontSize: 17, color: '#009387'}}>Role:</Text> {role}
              </Caption>
            </View>
            <View style= {{height: 40, justifyContent: 'center'}}>
              <Caption style={styles.caption2}>
                <Text style={{fontSize: 17, color: '#009387'}}>Status:</Text> Account is {isVerified}
              </Caption>
            </View>
          </View>
        </View>
      </ScrollView>
    );
};

export default OthersProfileScreen;

const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor: '#fff'
    },
    header: {
        width: '100%',
        backgroundColor: '#009387',
        justifyContent: 'flex-end',
        paddingBottom: 50,
        borderBottomRightRadius: 70,
    },
    titleBar: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginTop: Platform.OS === 'ios' ? 25 : 0,
      paddingHorizontal: 20,
      marginHorizontal: 5,
    },
    footer: {
        flex: 3,
        backgroundColor: '#009387',
    },
    footerinside: {
      flex: 1,
      backgroundColor: '#fff',
      borderTopLeftRadius: 70,
      paddingHorizontal: 20,
      paddingVertical: 30
  },
    text_header: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 30
    },
    title: {
      fontSize: 16,
      marginTop: 3,
      paddingTop: 20,
      fontWeight: 'bold',
      alignSelf: 'center',
      color: '#fff',
      textTransform: 'capitalize'
    },
    caption: {
      fontSize: 14,
      paddingBottom: 4,
      lineHeight: 14,
      color: '#fff',
      alignSelf: 'center'
    },
    caption2: {
      fontSize: 14,
      paddingTop: 4,
      lineHeight: 14,
      color: '#009387',
      // alignSelf: 'center'
    },
    pic: {
      height: '100%',
      width: '100%',
      borderRadius: 70
    },
  });
import React, {useState, useEffect} from 'react';
import { View, StyleSheet, StatusBar } from 'react-native';
import {
    Avatar,
    Title,
    Caption,
    Paragraph,
    Drawer,
    Text,
    TouchableRipple,
    Switch
} from 'react-native-paper';
import {
    DrawerContentScrollView,
    DrawerItem
} from '@react-navigation/drawer';
import { useSelector, useDispatch } from 'react-redux'
import { isLogOut } from '../redux/user/actions';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { TouchableOpacity } from 'react-native-gesture-handler';


export function DrawerContent(props) {
    const loginReducers = useSelector(state => state.loginReducers.data)
    const dispatch = useDispatch();

    return(
        <View style={{flex:1}}>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>
                    <View style={styles.userInfoSection}>
                        <View style={{flexDirection:'row',marginTop: 10}}>
                            <Avatar.Image 
                                source={{
                                    uri: 'https://api.adorable.io/avatars/50/abott@adorable.png'
                                }}
                                size={50}
                            />
                            <View style={{marginLeft:15, flexDirection:'column', flexShrink: 1}}>
                                <Title style={styles.title}>{loginReducers.data.firstname} {loginReducers.data.lastname}</Title>
                                <Caption style={styles.caption2}>{loginReducers.data.username}</Caption>
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.section}>
                                <Paragraph style={[styles.paragraph, styles.caption]}>Status:</Paragraph>
                                <Caption style={styles.caption}>{loginReducers.data.isVerified}</Caption>
                            </View>
                            <View style={styles.section}>
                                <Icon 
                                style={{marginBottom:3, paddingRight: 2, marginLeft: 5}}
                                name="check-circle-outline" 
                                color='green'
                                size={20}
                                />
                                <Caption style={styles.caption1}>{loginReducers.data.role}</Caption>
                            </View>
                        </View>
                    </View>

                    <Drawer.Section style={styles.drawerSection}>
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="account-outline" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="My Account"
                            onPress={() => {props.navigation.navigate('MyProfileScreen')}}
                        />
                        {
                            (loginReducers.data.role === 'Guest') ?
                            <DrawerItem 
                                icon={({color, size}) => (
                                    <Icon 
                                    name="plus-network-outline" 
                                    color={color}
                                    size={size}
                                    />
                                )}
                                label="Bid Requests"
                                onPress={() => {props.navigation.navigate('BidRequestScreen')}}
                            />
                            : 
                            null
                        }
                        {
                            (loginReducers.data.role === 'Guest') ?
                            <DrawerItem 
                                icon={({color, size}) => (
                                    <Icon 
                                    name="alpha-h-box" 
                                    color={color}
                                    size={size}
                                    />
                                )}
                                label="History"
                                onPress={() => {props.navigation.navigate('HistoryScreen')}}
                            />
                            : 
                            null
                        }
                    </Drawer.Section>
                </View>
            </DrawerContentScrollView>
            <Drawer.Section style={styles.bottomDrawerSection}>
                <TouchableOpacity onPress={()=> dispatch(isLogOut())}>
                <DrawerItem 
                    icon={({color, size}) => (
                        <Icon 
                        name="exit-to-app" 
                        color={color}
                        size={size}
                        />
                    )}
                    label="Sign Out"
                />
                </TouchableOpacity>
            </Drawer.Section>
        </View>
    );
}

const styles = StyleSheet.create({
    drawerContent: {
      flex: 1,
    },
    userInfoSection: {
      paddingLeft: 20,
    },
    title: {
      fontSize: 16,
      marginTop: 3,
      fontWeight: 'bold',
      textTransform: 'capitalize'
    },
    caption: {
      fontSize: 14,
      lineHeight: 14,
      paddingHorizontal: 1
    },
    caption2: {
        fontSize: 14,
        lineHeight: 14,
        paddingRight: 3
      },
    caption1: {
        fontSize: 14,
        lineHeight: 14,
        color: 'green' 
    },
    row: {
      marginTop: 20,
      flexDirection: 'row',
      alignItems: 'center',
    },
    section: {
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: 15,
    },
    paragraph: {
      fontWeight: 'bold',
      marginRight: 1,
    },
    drawerSection: {
      marginTop: 15,
      marginBottom: 0,
      borderTopColor: '#f4f4f4',
      borderTopWidth: 1,
      backgroundColor: '#fff'
    },
    bottomDrawerSection: {
        marginBottom: 2,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1,
        backgroundColor: '#fff'

    },
    preference: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical: 12,
      paddingHorizontal: 16,
    },

    themeText: {
        paddingTop: 5,
        color: '#85878a',
        fontWeight: 'bold'
    }
  });
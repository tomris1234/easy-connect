import React from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    TextInput,
    Platform,
    StyleSheet ,
    StatusBar,
    ScrollView
} from 'react-native';
import {
  Avatar,
  Title,
  Caption,
} from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import { useSelector, useDispatch } from 'react-redux'

import Summary from '../components/Summary';
import Skills from '../components/Skills';

const MyProfileScreen = ({navigation}) => {
  const loginReducers = useSelector(state => state.loginReducers.data)

    return (
      <ScrollView contentContainerStyle={styles.container}>
          <StatusBar backgroundColor='#009387' barStyle="light-content"/>
        <View style={styles.header}>
            <View style={styles.titleBar}>
              <TouchableOpacity onPress={()=> navigation.goBack()}>
                <Icon name="ios-arrow-back" color='#dcdcdc' size={30} />
              </TouchableOpacity>
              <Text style={{color: '#fff', paddingTop: 6, fontSize: 18}}>Profile</Text>
              <Icon name="md-more" color='#009387' size={32} />
            </View>
            <View style={{width: '100%', borderWidth: 0.4, borderColor: '#fff', marginHorizontal: 0}} />
            <View style={{flexDirection:'column',marginTop: 20,}}>
                <Avatar.Image 
                    source={{
                        uri: 'https://api.adorable.io/avatars/50/abott@adorable.png'
                    }}
                    style={{alignSelf: 'center'}}
                    size={120}
                />
                <View style={{marginLeft:15, flexDirection:'column'}}>
                    <Title style={styles.title}>{loginReducers.data.firstname} {loginReducers.data.lastname}</Title>
                    <Caption style={styles.captionu}>{loginReducers.data.username}</Caption>
                </View>
            </View>
        </View>
        <View style={styles.footer}>
          <View style={styles.footerinside}>
            {/* <Summary from={true}/> */}
            {/* <Skills from={true}/> */}
            <View style={styles.each1}>
              <Caption style={styles.caption}>
                <Text style={{fontSize: 17, color: '#009387', paddingTop: 6}}>Phone:</Text>
                {loginReducers.data.phone}</Caption>
            </View>
            <View style={styles.each}>
              <Caption style={styles.caption}>
                <Text style={{fontSize: 17, color: '#009387'}}>Status:</Text> {loginReducers.data.isVerified}
              </Caption>
            </View>
            <View style={styles.each}>
              <Caption style={styles.caption}>
                <Text style={{fontSize: 17, color: '#009387'}}>Role:</Text> {loginReducers.data.role}
              </Caption>
            </View>
          </View>
        </View>
      </ScrollView>
    );
};

export default MyProfileScreen;

const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor: '#fff'
    },
    header: {
        backgroundColor: '#009387',
        justifyContent: 'flex-end',
        paddingBottom: 50,
        borderBottomRightRadius: 70,
    },
    titleBar: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingHorizontal: 20,
      marginTop: Platform.OS === 'ios' ? 25 : 0,
      marginHorizontal: 5
    },
    footer: {
        flex: 3,
        backgroundColor: '#009387',
    },
    footerinside: {
      flex: 1,
      backgroundColor: '#fff',
      borderTopLeftRadius: 70,
      paddingHorizontal: 20,
      paddingVertical: 30
  },
    each: {
      height: 40,
      justifyContent: 'center'
    },
    each1: {
      marginTop: 20,
      height: 40,
      justifyContent: 'center'
    },
    text_header: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 30
    },
    title: {
      fontSize: 16,
      marginTop: 3,
      paddingTop: 20,
      fontWeight: 'bold',
      alignSelf: 'center',
      color: '#fff',
      textTransform: 'capitalize'
    },
    caption: {
      fontSize: 13,
      paddingTop: 4,
      lineHeight: 14,
      color: '#009387',
      // alignSelf: 'center',
      alignItems: 'center'
    },

    captionu: {
      fontSize: 14,
      paddingTop: 4,
      lineHeight: 14,
      color: '#fff',
      alignSelf: 'center',
      // alignItems: 'center'
    },
  });
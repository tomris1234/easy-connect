import React, {useState, useEffect} from 'react';
import { View, Text, ScrollView, StyleSheet, TouchableOpacity, TextInput, Alert, ActivityIndicator } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'

const PostJobScreen = ({navigation}) => {
  const [loading, setLoad] = React.useState(false);
  const [data, setData] = React.useState({
    title: '',
    requirement: [],
    description: '',
    location: '',
    minPrice: '',
    maxPrice: '',
});

const loginReducers = useSelector(state => state.loginReducers.data)


  const makePost = () => {
    // console.log(data, 'rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr')
    const url = `https://ibm-challenge-api.herokuapp.com/api/v1/jobs`;
    setLoad(true);
    fetch(url,
      {
        method: 'POST',
        headers: {  // these could be different for your API call
          'Content-Type': 'application/json',
          'token': loginReducers.token
        },
        body: JSON.stringify({
          "title": data.title,
          "description": data.description,
          "requirements": data.requirement,
          "location": data.location,
          "minPrice": data.minPrice,
          "maxPrice": data.maxPrice
      }),
      })
      .then((response) => response.json())
      .then((json) => {
        // console.log(json, 'created kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk jobs');
        if (json.success === true) { // response success checking logic could differ
          // console.log(json.data, 'cccccccc job dtas');
          Alert.alert('Success', 'Successfully created job', [
            {text: 'Ok'}
          ])
          setLoad(false)
          navigation.goBack();
          }
        else{
          Alert.alert('Error', 'Some field is missing', [
            {text: 'Ok'}
          ])
          setLoad(false)
        }
      })
      .catch((err) => {
        console.log(err);
          Alert.alert('Error', err, [
          {text: 'Ok'}
        ])
        setLoad(false)
      });
  };

    const titleInputChange = (val) => {
      setData({
        ...data,
        title: val,
      });
    }
    const descInputChange = (val) => {
      setData({
        ...data,
        description: val,
      });
    }
    const reqInputChange = (val) => {
      setData({
        ...data,
        requirement: val,
      });
    }
    const locInputChange = (val) => {
      setData({
        ...data,
        location: val,
      });

    }
    const minInputChange = (val) => {
      setData({
        ...data,
        minPrice: val,
      });
    }
    const maxInputChange = (val) => {
      setData({
        ...data,
        maxPrice: val,
      });
  }

    return (
        <>
        <ScrollView contentContainerStyle={styles.container}
        keyboardShouldPersistTaps='handled'>
            <View style={styles.jobBox}>
              <View style={styles.box}>
                <Text style={styles.post}> Title of job </Text>
                <TextInput 
                  placeholder="Title of job"
                  placeholderTextColor="#9E9E9E"
                  style={styles.textInput}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  onChangeText={(val) => titleInputChange(val)}
                />
              </View>
              <View style={styles.box}>
                <Text style={styles.post}> Description: </Text>
                <TextInput
                style={styles.TextInputStyleClass}
                underlineColorAndroid="transparent"
                placeholder={"Describe your job"}
                placeholderTextColor={"#9E9E9E"}
                textAlignVertical={'top'}
                numberOfLines={5}
                multiline={true}
                onChangeText={(val) => descInputChange(val)}
              />
              </View>
              <View style={styles.box}>
                <Text style={styles.post}> Requirements: </Text>
                <TextInput
                style={styles.TextInputStyleClass}
                underlineColorAndroid="transparent"
                placeholder={"Requirement for job"}
                placeholderTextColor={"#9E9E9E"}
                textAlignVertical={'top'}
                numberOfLines={5}
                multiline={true}
                onChangeText={(val) => reqInputChange(val)}
              />
              </View>
              <View style={styles.box}>
                <Text style={styles.post}> Location </Text>
                <TextInput 
                  placeholder="Location"
                  placeholderTextColor="#9E9E9E"
                  style={styles.textInput}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  onChangeText={(val) => locInputChange(val)}
                />
              </View>
              <View style={styles.box}>
                <Text style={styles.post}> Minimum Price:</Text>
                <TextInput 
                  placeholder="$10 /Hour"
                  placeholderTextColor="#9E9E9E"
                  style={styles.textInput}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  onChangeText={(val) => minInputChange(val)}
                />
              </View>
              <View style={[styles.box, {marginBottom: 8}]}>
                <Text style={styles.post}> Maximun Price:</Text>
                <TextInput 
                  placeholder="$12 /Hour"
                  placeholderTextColor="#9E9E9E"
                  style={styles.textInput}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  onChangeText={(val) => maxInputChange(val)}
                />
              </View>
            </View>
          <TouchableOpacity style={styles.postJob} onPress= {() => makePost()}>
              <Text style={styles.postJobText}> Post Job </Text>
          </TouchableOpacity>
          <View style={{height: 50, marginTop: 8}}>
          {loading ? <ActivityIndicator size="small" color="#009387" style={{marginBottom: 50}}/> : null}
          </View>
        </ScrollView>
        </>
    );
};

export default PostJobScreen;

const styles = StyleSheet.create({
  container: {
    flexGrow: 1, 
    alignItems: 'center', 
    // justifyContent: 'center'
  },

  jobBox: {
    marginTop: 5,
    width: '98%',
    borderWidth: 1,
    borderColor: '#009387',
    backgroundColor: '#fff',
    borderRadius: 5
  },

  box: {
    minHeight: 72,
  },

  post: {
    padding: 8
  },

  textInput: {
    height: 45,
    width: '94%',
    // marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
    borderWidth: 1,
    alignSelf: 'center',
    borderColor: '#009387',
    backgroundColor: '#eff0f1',
    borderRadius: 10
},

TextInputStyleClass: {
  // marginLeft: 4,
  minHeight: 160,
  alignSelf: 'center',
  width: '94%',
  // marginTop: Platform.OS === 'ios' ? 0 : -12,
  paddingLeft: 10,
  color: '#05375a',
  borderWidth: 1,
  borderColor: '#009387',
  backgroundColor: '#eff0f1',
  borderRadius: 10
},

  postJob: {
    margin: 8,
    alignSelf: 'center',
    height: 40,
    width: 120,
    borderRadius: 20,
    backgroundColor: '#009387',
    justifyContent: 'center',
    alignItems: 'center',
  },

  postJobText: {
    color: 'white',
    alignSelf: 'center',
  },
});
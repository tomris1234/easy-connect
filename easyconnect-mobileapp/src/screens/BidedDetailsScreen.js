import React, {useState, useEffect} from 'react';
import { View, Text, Button, StyleSheet, Alert, ActivityIndicator, StatusBar } from 'react-native';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useSelector, useDispatch } from 'react-redux';

const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

const BidedDetailsScreen = ({route}) => {
  const navigation = useNavigation();
  const [loading, setLoad] = useState(false);
  const [doData, setData] = useState('');
  const [acc, setAcc] = useState(false);

  const { specialNote } = route.params.item;
  const { student } = route.params.item;
  const { amount } = route.params.item;
  const { job } = route.params.item;
  const firstname  = route.params.firstname;
  const lastname  = route.params.lastname;
  const title  = route.params.title;
  const description  = route.params.description;

  const loginReducers = useSelector(state => state.loginReducers.data)

  useEffect(() => {
    getJob();
    // console.log(doData)
  }, [] )

  const getJob = () => {
    const url = `https://ibm-challenge-api.herokuapp.com/api/v1/jobs/${job}`;
    fetch(url,
      {
        method: 'GET',
        headers: { 
          'Content-Type': 'application/json',
          'token': loginReducers.token
        },
      })
      .then((response) => response.json())
      .then((json) => {
        // console.log(json, 'hiiiiiiiiiiiiiiii job one iiiiiiiiiiiiiiiiiiiiiiii');
        if (json.success === true) {
          setData(json.data)
        console.log(json.data.status, 'hiiiiiiiiiiiiiiii job one iiiiiiiiiiiiiiiiiiiiiiii');
          }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const acceptBid = () => {
    setAcc(true)
    const url = `https://ibm-challenge-api.herokuapp.com/api/v1/jobs/lockJoborUser`;
    setLoad(true);
    fetch(url,
      {
        method: 'POST',
        headers: {  // these could be different for your API call
          'Content-Type': 'application/json',
          'token': loginReducers.token
        },
        body: JSON.stringify({
          "jobId": job,
          "userId": student,
      }),
      })
      .then((response) => response.json())
      .then((json) => {
        console.log(json, 'biddinggggggg kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk jobs');
        if (json.success === true) { // response success checking logic could differ
          // console.log(json.data, 'biddinggggggggggggggggggggggggggg');
          Alert.alert('Success', json.message, [
            {text: 'Ok'}
          ])
          setLoad(false)
          }
        else{
          Alert.alert('Error', "Try again", [
            {text: 'Ok'}
          ])
          setLoad(false)
        }
      })
      .catch((err) => {
        console.log(err);
          Alert.alert('Error', err, [
          {text: 'Ok'}
        ])
        setLoad(false)
      });
  };

    return (
      <>

        <View style={styles.header}>
          <View style={styles.back}>
              <TouchableOpacity onPress={()=>navigation.goBack()} >
                <Icon name="ios-arrow-back" size={30} color="#fff" backgroundColor="#009387" style={{marginRight: 5}} />
              </TouchableOpacity>
          </View>
          <View style={styles.titleh}>
              <Text style={styles.titleTexth}> Bids Details </Text>
          </View>
          <View style={styles.more}>

          </View>
        </View>
        <ScrollView contentContainerStyle={styles.container} >

          <View style={styles.jobBox}>
            <View style={styles.title}>
              <Text style={styles.titleText}> {title} </Text>
            </View>
            <View style={styles.description}>
              <Text style={styles.descriptionHeader}> Description: </Text>
              <Text style={styles.descriptionLists}> {description} </Text>
            </View>
            <View style={styles.description}>
              <Text style={styles.descriptionHeader}> Cover Letter: </Text>
              <Text style={styles.descriptionLists}> {specialNote}</Text>
            </View>
            <View style={styles.post}>
              <Icons
                name="account-outline" 
                color='#009387'
                size={14}
                style={{paddingLeft: 0}}
                />
              <Text style={styles.postName}> <Text style={{color: '#009387'}}>Bided by: </Text>
                {firstname} {lastname}
              </Text>
            </View>
            <View style={styles.post}>
              <Icons
                name="circle" 
                color='#009387'
                size={12}
                />
              <Text style={styles.Amount}> <Text style={{color: '#009387', paddingRight: 5}}>Expected Price: </Text> 
                 {amount}
              </Text>
            </View>
            {/* <View style={styles.post}>
              <Icons
                name="circle" 
                color='#009387'
                size={12}
                />
              <Text style={styles.timeText}><Text style={{color: '#009387'}}> Working Hours :</Text> 40 Hours/week </Text>
            </View> */}
          </View>
          {/*TODO:: make receiver user _id dynamic*/}
          <TouchableOpacity style={styles.bid} onPress={()=>navigation.navigate('ChatScreen',{receiverUid: student})}>
              <Text style={styles.bidText}> Message </Text>
          </TouchableOpacity>
          { doData.status === 'Locked' || acc  ?
            <TouchableOpacity style={styles.bidA} >
              <Text style={styles.bidText}> Accept </Text>
            </TouchableOpacity>
          : 
            <TouchableOpacity style={styles.bid} onPress={()=> acceptBid()}>
              <Text style={styles.bidText}> Accept </Text>
            </TouchableOpacity>
          }

          {loading ? <ActivityIndicator size="small" color="#009387"
           style={{marginBottom: 50, marginTop: 8}}/> : null}

          {acc || doData.status === 'Locked'  ?
            <Text> This bid has been accepted. </Text>
          : 
            null
          }
        </ScrollView>
        </>
    );
};

export default BidedDetailsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    alignItems: 'center', 
  },

  jobBox: {
    marginTop: 5,
    width: '98%',
    borderWidth: 1,
    borderColor: '#009387',
    backgroundColor: '#eff0f1'
  },

  title: {
    marginLeft: 3,
    padding: 5
  },

  titleText: {
    fontSize: 17,
    fontWeight: 'bold',
    textTransform: 'capitalize'
  },

  description: {
    marginLeft: 3,
    minHeight: 60,
  },

  descriptionHeader: {
    padding: 5,
    fontSize: 14,
    fontWeight: 'bold'
  },

  descriptionLists: {
    paddingLeft: 8,
  },

  requirementList: {

  },

  post: {
    marginLeft: 10,
    height: 35,
    flexDirection: 'row',
    alignItems: 'center'
  },

  postName: {
    textTransform: 'capitalize'
  },

  Amount: {
    // paddingLeft: 5
  },

  timeText: {

  },
  
  bidShow: {

  },

  bid: {
    margin: 8,
    alignSelf: 'center',
    height: 40,
    width: 120,
    borderRadius: 20,
    backgroundColor: '#009387',
    justifyContent: 'center',
    alignItems: 'center',
  },

  bidA: {
    margin: 8,
    alignSelf: 'center',
    height: 40,
    width: 120,
    borderRadius: 20,
    backgroundColor: '#dcdcdc',
    justifyContent: 'center',
    alignItems: 'center',
  },

  bidText: {
    color: 'white',
    alignSelf: 'center',
  },

  //modal css
  bidding: {
    color: '#009387',
    fontSize: 18,
    fontWeight: 'bold',
    padding: 8
  },

  biddingTitle: {
    paddingLeft: 8,
  },

  Rate: {
    minHeight: 72,
    // marginLeft: 8,
  },

  rateTitle: {
    paddingLeft: 8,
    paddingTop: 8,
    paddingBottom: 5
  },

  textInput: {
    height: 45,
    width: '93%',
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
    borderWidth: 1,
    alignSelf: 'center',
    borderColor: '#009387',
    borderRadius: 10,
    backgroundColor: '#eff0f1'
},

TextInputStyleClass: {
  // marginLeft: 4,
  minHeight: 160,
  alignSelf: 'center',
  width: '93%',
  marginTop: Platform.OS === 'ios' ? 0 : -12,
  paddingLeft: 10,
  color: '#05375a',
  borderWidth: 1,
  borderColor: '#009387',
  borderRadius: 10,
  backgroundColor: '#eff0f1'
},

bidSubmit: {
  margin: 8,
  alignSelf: 'center',
  height: 40,
  width: 120,
  borderRadius: 20,
  backgroundColor: '#009387',
  justifyContent: 'center',
  alignItems: 'center',
},

  //header
  header: {
    marginBottom: 5,
    height: 72,
    width: '100%',
    alignItems: 'center', 
    flexDirection: 'row',
    marginTop: Platform.OS === 'ios' ? 0 : -28,
    backgroundColor: '#009387'
  },

  back: {
    marginTop: STATUS_BAR_HEIGHT,
    height: '100%',
    width: '18%',
    justifyContent: 'center',
    alignItems: 'center'
  },

  titleh: {
    marginTop: STATUS_BAR_HEIGHT,
    height: '100%',
    width: '62%',
    justifyContent: 'center',
    alignItems: 'center'
  },

  titleTexth: {
    fontSize: 18,
    color: '#fff'
  },

  more: {
    marginTop: STATUS_BAR_HEIGHT,
    height: '100%',
    width: '20%',
  },

});
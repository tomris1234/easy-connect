import React, { useState, useCallback, useEffect } from 'react';
import { View} from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat'
import firebaseSDK from "../firebase/firebaseSdk";
import {useSelector} from "react-redux";
import MessageRequest from "../../Assistant";
const ChatScreen = ({route}) => {
  const [messages, setMessages] = useState([]);
  const user =  useSelector(state => state.loginReducers.data)
  useEffect(() => {
      loadMessages(chatID())
  }, [])
  const { receiverUid, chatId } = route.params;


  const chatID = () => {
    if(chatId === undefined) {
      const senderId = user.data._id;
      const receiverId = receiverUid;
      const chatIDpre = [];
      chatIDpre.push(senderId);
      chatIDpre.push(receiverId);
      chatIDpre.sort();
      return chatIDpre.join('_');
    }else{
      return  chatId;
    }
  };

  function loadMessages(chat_id){
      const chatRef = firebaseSDK.ChatRef(chat_id);
      chatRef.on("value", snap => {
        let messages = [];
        snap.forEach(child => {
          messages.push({...child.val()})
        })

        setMessages(prevState => messages.reverse())
      });
  }
  const onSend = useCallback((messages = []) => {
    messages.forEach((message) => {
      message.uidFrom =user.data._id;
      message.uidTo =  receiverUid;
      message.chat_id =  chatID();
      firebaseSDK.sendMessage(chatID(),message, receiverUid);
      if(user.data.role === "Student") {
          getMessage(message.text);
      }
    });
  }, [])

    const getMessage = async (text) => {
        let response = await MessageRequest(text);
        if(response.output.text[0] !== undefined){
            let message = {
                _id: Math.round(Math.random() * 1000000).toString(),
                text: response.output.text.join(' '),
                createdAt: new Date(),
                user: {
                    _id: receiverUid,
                    name: 'Watson Assistant',
                },
            };
            message.uidFrom =  receiverUid;
            message.uidTo =user.data._id;
            message.chat_id =  chatID();
            console.log('sending', message);
            firebaseSDK.sendMessage(chatID(),message);
        }
    }

  return (
    <View style={{flex: 1}}>
        <GiftedChat
        messages={messages}
        alwaysShowSend={true}
        bottomOffset={-10}
        textInputProps={{ minHeight: 44, borderWidth: 1, borderColor: '#009387', borderRadius: 5,}}
        textInputStyle={{padding: 8, paddingTop: 12}}
        onSend={messages => onSend(messages)}
        user={{
            _id: user.data._id,
            name : user.data.username,
            avatar: ''
        }}
        />
    </View>
  )
}

export default ChatScreen;
import React, {useState, useEffect} from 'react';
import { View, SafeAreaView, FlatList,
  ActivityIndicator, StyleSheet, TouchableOpacity, Text, StatusBar } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import Icon from 'react-native-vector-icons/Ionicons';

import BidReq from '../components/BidReq';
import contact from '../../assets/contact.png';
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

const BidRequestScreen = ({navigation}) => {
  const [data, setData] = useState();
  const [loading, setLoad] = React.useState(false);
  const [isFetching, setFetch] = React.useState(false);
  const loginReducers = useSelector(state => state.loginReducers.data)

  useEffect(() => {
    makeRequest()
    console.log(data, 'data in bidrequest')
    console.log(loginReducers.data.id, 'data in bidrequest')
  }, []);

 const onRefresh = () => {
  setFetch(true);
  makeRequest();
}

  const makeRequest = () => {
    const url = `https://ibm-challenge-api.herokuapp.com/api/v1/task/guest/${loginReducers.data.id}`;
    setLoad(true);
    fetch(url,
      {
        method: 'GET',
        headers: {  // these could be different for your API call
          'Content-Type': 'application/json',
          'token': loginReducers.token
        },
      })
      .then((response) => response.json())
      .then((json) => {
        if (json.success === true) { // response success checking logic could differ
          setData(json.data)
          setLoad(false)
          setFetch(false);
        }
        setLoad(false)
        setFetch(false);
      })
      .catch((err) => {
        console.log(err);
        setLoad(false)
        setFetch(false);
      });
  };

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.back}>
              <TouchableOpacity onPress={()=>navigation.goBack()} >
                <Icon name="ios-arrow-back" size={30} color="#fff" backgroundColor="#009387" style={{marginRight: 5}} />
              </TouchableOpacity>
          </View>
          <View style={styles.title}>
              <Text style={styles.titleText}> Bids Request </Text>
          </View>
          <View style={styles.more}>

          </View>
        </View>
        {loading ? <ActivityIndicator size="small" color="#009387" style={{margin: 8}}/> : null}
        <SafeAreaView style={{flex:1}}>
          <FlatList
            data={data}
            refreshing={isFetching}
            onRefresh={() => onRefresh()}
            renderItem={({ item }) => (
              <BidReq
                item={item}
                contact={contact}
                key={item.id}
              />
            )}
          />
        </SafeAreaView >
      </View>
    );
};

export default BidRequestScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    alignItems: 'center', 
    // justifyContent: 'center'
  },

  //header
  header: {
    marginBottom: 5,
    height: 72,
    width: '100%',
    alignItems: 'center', 
    flexDirection: 'row',
    marginTop: Platform.OS === 'ios' ? 0 : -28,
    backgroundColor: '#009387'
  },

  back: {
    marginTop: STATUS_BAR_HEIGHT,
    height: '100%',
    width: '18%',
    justifyContent: 'center',
    alignItems: 'center'
  },

  title: {
    marginTop: STATUS_BAR_HEIGHT,
    height: '100%',
    width: '62%',
    justifyContent: 'center',
    alignItems: 'center'
  },

  titleText: {
    fontSize: 18,
    color: '#fff'
  },

  more: {
    marginTop: STATUS_BAR_HEIGHT,
    height: '100%',
    width: '20%',
  },


  scrollStyle: {
    // flex: 1,
    width: '100%'

  }
});
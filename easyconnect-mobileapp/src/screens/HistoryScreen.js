import React, {useState, useEffect} from 'react';
import { View, Text, FlatList, StyleSheet, SafeAreaView, ActivityIndicator } from 'react-native';
import DHeader from '../components/DHeader';
import JobPost from '../components/JobPost';
import { useSelector, useDispatch } from 'react-redux'

const HistoryScreen = ({navigation}) => {
  const [loading, setLoad] = React.useState(false);
  const [data, setData] = useState();
  const loginReducers = useSelector(state => state.loginReducers.data)

  useEffect(() => {
    makeRequest()
  }, []);

  const makeRequest = () => {
    const url = `https://ibm-challenge-api.herokuapp.com/api/v1/jobs`;
    setLoad(true);
    fetch(url,
      {
        method: 'GET',
        headers: { 
          'Content-Type': 'application/json',
          'token': loginReducers.token
        },
      })
      .then((response) => response.json())
      .then((json) => {
        console.log(json, 'data from api list')
        if (json.success === true) {
          setData(json.data.reverse())
          setLoad(false)
        }
        setLoad(false)
      })
      .catch((err) => {
        console.log(err);
        setLoad(false)
      });
  };

    return (
      <View style={styles.container}>
        <DHeader title="History"/>
        <View style={styles.header}>
          <Text style={styles.headerText}> Jobs you have posted </Text>
        </View>
        {loading ? <ActivityIndicator size="small" color="#009387" style={{margin: 8}}/> : null}
        <SafeAreaView style={{flex:1}}>
          <FlatList
            data={data}
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => (
              <JobPost
                item={item}
                key={item.id}
              />
            )}
          />
        </SafeAreaView >
      </View>
    );
};

export default HistoryScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    alignItems: 'center', 
    backgroundColor: '#d8d8d8'
  },

  header: {
    width: '100%',
    height: 40,
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderColor: '#009387'
  },

  headerText: {
    fontSize: 20,
    color: '#009387',
    alignSelf: 'center'
  },
});
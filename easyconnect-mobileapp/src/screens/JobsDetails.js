import React, {useState, useEffect} from 'react';
import { View, Text, ScrollView, StyleSheet, TextInput, Keyboard,
  TouchableOpacity, TouchableWithoutFeedback, ActivityIndicator, Alert } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';
import Modal from 'react-native-modal';

const JobsDetails = ({route}) => {
  const navigation = useNavigation();
  const [visible, setVisible] = useState(false);
  const [loading, setLoad] = useState(false);

  const { title } = route.params.item;
  const { description } = route.params.item;
  const { requirements } = route.params.item;
  const  firstname  = route.params.firstname;
  const  lastname  = route.params.lastname;
  const { minPrice } = route.params.item;
  const { maxPrice } = route.params.item;
  const { bidders } = route.params.item;
  const { createdBy } = route.params.item;
  const { id } = route.params.item;
  const { status } = route.params.item;

  const loginReducers = useSelector(state => state.loginReducers.data)

  const [data, setData] = useState({
    job: '',
    student: '',
    guest: '',
    amount: '',
    specialNote: '',
  });

  useEffect(() => {
    setData({
      ...data,
      job: id,
      student: loginReducers.data._id,
      guest: createdBy,
  });
  }, [] )

  const rateInputChange = (val) => {
    setData({
        ...data,
        amount: val,
    });
  }

  const coverInputChange = (val) => {
    setData({
        ...data,
        specialNote: val,
    });
  }

  const openModal = () => {
    setVisible(!visible);
  }

  const makeBid = () => {
    const url = `https://ibm-challenge-api.herokuapp.com/api/v1/task`;
    setLoad(true);
    fetch(url,
      {
        method: 'POST',
        headers: {  // these could be different for your API call
          'Content-Type': 'application/json',
          'token': loginReducers.token
        },
        body: JSON.stringify({
          "job": id,
          "amount": data.amount,
          "student": loginReducers.data._id,
          "guest": createdBy,
          "specialNote": data.specialNote,
      }),
      })
      .then((response) => response.json())
      .then((json) => {
        console.log(data)
        if (json.success === true) {
          Alert.alert('Success', 'Successfully bidded job', [
            {text: 'Ok'}
          ])
          setLoad(false)
          navigation.goBack();
          }
          else{
            if (json.message === 'User Already Place Bid') {
            Alert.alert('Error','User Already Place Bid', [
              {text: 'Ok'}
            ])
          }
          else{
            Alert.alert('Error','Fill all field', [
              {text: 'Ok'}
            ])
          }
            setLoad(false)
          }
      })
      .catch((err) => {
        console.log(err);
          Alert.alert('Error', "Network error", [
          {text: 'Ok'}
        ])
        setLoad(false)
      });
  };

  const showModal = () => {
    return(
      <Modal 
      animationIn="slideInUp"
      isVisible={visible}
      onBackdropPress={()=> openModal()}
      onBackButtonPress={()=> openModal()}
      style={{flex:1, justifyContent: 'flex-end', alignItems: 'center', }}
      >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <View style={{height: '90%', width: '100%', backgroundColor: '#fff'}}>
            <ScrollView>
              <Text style={styles.bidding}> Place a Bid on </Text>
              <Text style={styles.biddingTitle}> Title of job </Text>
              <View style={styles.Rate}>
                <Text style={styles.rateTitle}> Expected Rate </Text>
                <TextInput 
                  placeholder="Expected Rate"
                  placeholderTextColor="#9E9E9E"
                  style={styles.textInput}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  onChangeText={(val) => rateInputChange(val)}
                />
              </View>
              <View style={styles.Rate}>
                <Text style={styles.rateTitle}> Cover your Bid </Text>
                <TextInput
                style={styles.TextInputStyleClass}
                underlineColorAndroid="transparent"
                placeholder={"Cover Your bid"}
                placeholderTextColor={"#9E9E9E"}
                textAlignVertical={'top'}
                numberOfLines={5}
                multiline={true}
                onChangeText={(val) => coverInputChange(val)}
              />
              </View>
              <TouchableOpacity style={styles.bidSubmit} onPress={()=> makeBid()}>
                  <Text style={styles.bidText}> Submit Bid </Text>
              </TouchableOpacity>
              <View style={{height: 50, marginTop: 8}}>
                {loading ? <ActivityIndicator size="small" color="#009387" style={{marginBottom: 50}}/> : null}
              </View>
            </ScrollView>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
  
    return (
      <>
        <ScrollView contentContainerStyle={styles.container} >
          <View style={styles.jobBox}>
            <View style={styles.title}>
              <Text style={styles.titleText}> {title} </Text>
            </View>
            <View style={styles.description}>
              <Text style={styles.descriptionHeader}> Description: </Text>
              <Text style={styles.descriptionLists}> {description} </Text>
            </View>
            <View style={styles.description}>
              <Text style={styles.descriptionHeader}> Requirements: </Text>
              {
                requirements.map((item,i) => <Text style={styles.descriptionLists}>{i+1}. {item} </Text>)
              }
            </View>
            <View style={styles.post}>
              <Icons
                name="account-outline" 
                color='#009387'
                size={14}
                style={{paddingLeft: 0}}
                />
              <Text style={styles.postName}> {firstname} {lastname} </Text>
            </View>
            <View style={styles.post}>
              <Icons
                name="circle" 
                color='#009387'
                size={12}
                />
              <Text style={styles.Amount}> Price: {minPrice}$-{maxPrice}$</Text>
            </View>
            <View style={styles.post}>
              <Icons
                name="circle" 
                color='#009387'
                size={12}
                />
              <Text style={styles.timeText}> 7 days left </Text>
            </View>
            <View style={styles.post}>
              <Icons
                name="circle" 
                color='#009387'
                size={12}
                />
              <Text style={styles.bidShow}> 3 Months</Text>
            </View>
            <View style={styles.post}>
              <Icons
                name="circle" 
                color='#009387'
                size={12}
                />
              <Text style={styles.bidShow}> {bidders.length} bids</Text>
            </View>
            {status !== 'Open' ?
            <View style={styles.post}>
              <Icons
                name="circle" 
                color='#009387'
                size={12}
                />
              <Text style={styles.Notice}> This job is no more available </Text>
            </View>
            :
            null  
            }
          </View>
          {
            (loginReducers.data.role === 'Student' && status === 'Open' ) ?
            <TouchableOpacity style={styles.bid} onPress={()=> openModal()}>
              <Text style={styles.bidText}> Bid </Text>
            </TouchableOpacity> 
            :
            <TouchableOpacity style={styles.bids} onPress={()=> 
              Alert.alert('Sorry', 'Only Students can bid or Job may be closed', [
              {text: 'Ok'}
              ])}>
              <Text style={styles.bidText}> Bid </Text>
            </TouchableOpacity> 
          }
        </ScrollView>
        {showModal()}
        </>
    );
};

export default JobsDetails;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    alignItems: 'center', 
    // justifyContent: 'center'
  },

  jobBox: {
    marginTop: 5,
    width: '98%',
    borderWidth: 1,
    borderColor: '#009387',
    backgroundColor: '#eff0f1'
  },

  title: {
    marginLeft: 3,
    padding: 5
  },

  titleText: {
    fontSize: 17,
    fontWeight: 'bold',
    textTransform: 'capitalize'
  },

  description: {
    marginLeft: 3,
    minHeight: 50,
  },

  descriptionHeader: {
    paddingLeft: 5,
    paddingTop: 0,
    fontSize: 14,
    fontWeight: 'bold'
  },

  descriptionLists: {
    paddingLeft: 8,
  },

  requirementList: {

  },

  post: {
    marginLeft: 10,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center'
  },

  postName: {
    textTransform: 'capitalize'
  },

  Amount: {

  },

  timeText: {

  },
  
  bidShow: {

  },

  Notice: {
    color: 'red'
  },

  bid: {
    margin: 8,
    alignSelf: 'center',
    height: 40,
    width: 120,
    borderRadius: 20,
    backgroundColor: '#009387',
    justifyContent: 'center',
    alignItems: 'center',
  },

  bids: {
    margin: 8,
    alignSelf: 'center',
    height: 40,
    width: 120,
    borderRadius: 20,
    backgroundColor: '#777',
    justifyContent: 'center',
    alignItems: 'center',
  },

  bidText: {
    color: 'white',
    alignSelf: 'center',
  },

  //modal css
  bidding: {
    color: '#009387',
    fontSize: 18,
    fontWeight: 'bold',
    padding: 8
  },

  biddingTitle: {
    paddingLeft: 8,
  },

  Rate: {
    minHeight: 72,
    // marginLeft: 8,
  },

  rateTitle: {
    paddingLeft: 8,
    paddingTop: 8,
    paddingBottom: 5
  },

  textInput: {
    height: 45,
    width: '93%',
    // marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
    borderWidth: 1,
    alignSelf: 'center',
    borderColor: '#009387',
    borderRadius: 10,
    backgroundColor: '#eff0f1'
},

TextInputStyleClass: {
  // marginLeft: 4,
  minHeight: 160,
  alignSelf: 'center',
  width: '93%',
  // marginTop: Platform.OS === 'ios' ? 0 : -12,
  paddingLeft: 10,
  color: '#05375a',
  borderWidth: 1,
  borderColor: '#009387',
  borderRadius: 10,
  backgroundColor: '#eff0f1'
},

bidSubmit: {
  margin: 8,
  alignSelf: 'center',
  height: 40,
  width: 120,
  borderRadius: 20,
  backgroundColor: '#009387',
  justifyContent: 'center',
  alignItems: 'center',
}

});
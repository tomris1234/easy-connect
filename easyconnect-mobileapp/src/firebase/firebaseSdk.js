import firebase from 'firebase';

class FirebaseSDK {
    constructor() {
        if (!firebase.apps.length) {
            //avoid re-initializing
            firebase.initializeApp({
                apiKey: "AIzaSyAPHfrxaBDNO9ArG7rGMsWuqueGILFglJs",
                authDomain: "disable-ibm.firebaseapp.com",
                databaseURL: "https://disable-ibm.firebaseio.com",
                projectId: "disable-ibm",
                storageBucket: "disable-ibm.appspot.com",
                messagingSenderId: "130756819221",
                appId: "1:130756819221:web:f5ea2cb18442acb6b1911c",
                measurementId: "G-600PQXXRDS"
            });
        }
    }

    ChatRef = (chatId) => {
        return firebase.database().ref(`chat/${chatId}`);
    }

    ChatUserRef = (userId) => {
        return firebase.database().ref(`chatUser/${userId}`);
    }
    sendMessage =  (chatId,message) => {
        const chatRef = firebase.database().ref(`chat/${chatId}`)
        chatRef.push(message);
        const chatUserRefFrom = firebase.database().ref(`chatUser/${message.uidFrom}/${chatId}`)
        chatUserRefFrom.set({'chat_id' : chatId});
        const chatUserRefTo = firebase.database().ref(`chatUser/${message.uidTo}/${chatId}`)
        chatUserRefTo.set({'chat_id' : chatId});
    };
}
const firebaseSDK = new FirebaseSDK();
export default firebaseSDK;
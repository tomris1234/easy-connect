import React, {useState, useEffect} from 'react';
import { View, Text, Button, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux'

const NotificationBox = (props) => {
  const navigation = useNavigation();
  const [sData, setSData] = useState('');
  const [gData, setGData] = useState('');
  const loginReducers = useSelector(state => state.loginReducers.data)

    useEffect(() => {
    makeRequestStudent()
    makeRequestGuest()
    // console.log(gData, 'job by id');

    // console.log(props.item, 'sadfdsfsdafdsddddddddddddddddddddddddddddddddd')
  }, []);

  const makeRequestStudent = () => {
    const url = `https://ibm-challenge-api.herokuapp.com/api/v1/users/profile/${props.item.student}`;
    fetch(url,
      {
        method: 'GET',
        headers: {  // these could be different for your API call
          'Content-Type': 'application/json',
          'token': loginReducers.token
        },
      })
      .then((response) => response.json())
      .then((json) => {
        // console.log(json, 'allll jobs llllllllllllllllllllllll');
        if (json.success === true) { // response success checking logic could differ
          setSData(json.data)
          // console.log(json.data, 'student details');
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

    const makeRequestGuest = () => {
    const url = `https://ibm-challenge-api.herokuapp.com/api/v1/jobs/${props.item.job}`;
    fetch(url,
      {
        method: 'GET',
        headers: {  // these could be different for your API call
          'Content-Type': 'application/json',
          'token': loginReducers.token
        },
      })
      .then((response) => response.json())
      .then((json) => {
        console.log(json, 'allll jobs');
        if (json.success === true) { // response success checking logic could differ
          setGData(json.data)
          // console.log(json.data, 'job by id');
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

    return (
      <TouchableOpacity style={{width: '100%', alignItems: 'center'}} onPress={()=> navigation.navigate('BidedDetailsScreen', 
      {
        title: gData.title,
        firstname: sData.firstname,
        lastname: sData.lastname,
        title: gData.title,
        description: gData.description,
        item: props.item
      }
      )}>
        <View style={styles.container} >
          <View style={styles.icon}>
              <View style={styles.logo}>
                  <Image source={props.contact} style={styles.pic}/>
              </View>
          </View>
          <View style={styles.details}>
              <Text style={styles.topText}>{sData.username } bidded on your job</Text>
              {gData ?
                <Text style={styles.bottomText}>{gData.title}</Text>
                : null
              }
          </View>
          <View style={styles.arrow}>
            <Icon name="ios-arrow-forward" color='#009387' size={26} />
          </View>
        </View>
      </TouchableOpacity>
    );
};

export default NotificationBox;

const styles = StyleSheet.create({
  container: {
    marginTop: 4,
    height: 72,
    width: '97%',
    borderWidth: 1,
    flexDirection: 'row',
    borderColor: '#009387',
    alignItems: 'center', 
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderRadius: 4
  },

  icon: {
    width: '20%',
    justifyContent: 'center',
    alignItems: 'center'
  },

  logo: {
    height: 55,
    width: 55,
    backgroundColor: '#009387',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center'
  },

  pic: {
    height: 25,
    width: 25,
  },

  details: {
    height: '100%',
    width: '70%',
    justifyContent: 'center'
  },

  topText: {
    padding: 2,
    fontSize: 16
  },

  bottomText: {
    padding: 2,
    fontSize: 12,
    textTransform: 'capitalize'
  },

  arrow: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center'
  },
});
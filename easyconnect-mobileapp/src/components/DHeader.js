import React from 'react';
import { View, Text, Button, StyleSheet, StatusBar, } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';

const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

const BidsComponent = (props) => {
  const navigation = useNavigation();
    console.log(StatusBar.currentHeight)
    return (
      <View style={styles.container}>
        <View style={styles.back}>
            <TouchableOpacity onPress={()=>navigation.goBack()} >
              <Icon name="ios-arrow-back" size={30} color="#fff" backgroundColor="#009387" style={{marginRight: 5}} />
            </TouchableOpacity>
        </View>
        <View style={styles.title}>
            <Text style={styles.titleText}> {props.title} </Text>
        </View>
        <View style={styles.more}>

        </View>
      </View>
    );
};

export default BidsComponent;

const styles = StyleSheet.create({
  container: {
    marginBottom: 5,
    height: 72,
    width: '100%',
    alignItems: 'center', 
    flexDirection: 'row',
    // justifyContent: 'center',
    backgroundColor: '#009387'
  },

  back: {
    marginTop: STATUS_BAR_HEIGHT,
    height: '100%',
    width: '18%',
    justifyContent: 'center',
    alignItems: 'center'
  },

  title: {
    marginTop: STATUS_BAR_HEIGHT,
    height: '100%',
    width: '62%',
    justifyContent: 'center',
    alignItems: 'center'
  },

  titleText: {
    fontSize: 18,
    color: '#fff'
  },

  more: {
    marginTop: STATUS_BAR_HEIGHT,
    height: '100%',
    width: '20%',
  },

});
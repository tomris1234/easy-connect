import React from 'react';
import { View, Text, Button, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useNavigation } from '@react-navigation/native';

const Message = (props) => {
  const navigation = useNavigation();
  let receiverId = props.messageDetail.user._id;
  const currentUserId = props.currentUserId;
  if(receiverId === currentUserId){
      receiverId = props.messageDetail.uidTo;
  }

    return (
      <TouchableOpacity style={styles.container} onPress={()=>navigation.navigate('ChatScreen',{receiverUid: receiverId, chatId: props.messageDetail.chat_id})}>
        <View style={styles.icon}>
            <View style={styles.logo}>
                <Image source={props.icon} style={styles.pic}/>
            </View>
        </View>
        <View style={styles.details}>
            <Text style={styles.topText}>{props.title} </Text>
            <Text style={styles.bottomText}>{props.details} </Text>
        </View>
        <View style={styles.arrow}>
          <Icon name="ios-arrow-forward" color='#009387' size={26} />
        </View>
      </TouchableOpacity>
    );
};

export default Message;

const styles = StyleSheet.create({
  container: {
    marginTop: 3,
    height: 72,
    width: '97%',
    borderWidth: 1,
    flexDirection: 'row',
    borderColor: '#009387',
    alignItems: 'center', 
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderRadius: 4
  },

  icon: {
    width: '20%',
    justifyContent: 'center',
    alignItems: 'center'
  },

  logo: {
    height: 55,
    width: 55,
    backgroundColor: '#009387',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center'
  },

  pic: {
    height: 25,
    width: 25,
  },

  details: {
    height: '100%',
    width: '70%',
    justifyContent: 'center'
  },

  topText: {
    padding: 2,
    fontSize: 16
  },

  bottomText: {
    padding: 2,
    fontSize: 12
  },

  arrow: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center'
  },
});
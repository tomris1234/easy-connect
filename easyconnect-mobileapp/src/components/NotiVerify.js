import React, {useState, useEffect} from 'react';
import { View, Text, Button, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const NotiVerify = (props) => {

    return (
      <TouchableOpacity style={{width: '100%', alignItems: 'center'}}>
        <View style={styles.container} >
          <View style={styles.icon}>
              <View style={styles.logo}>
                <Icon name="ios-arrow-forward" color='#fff' size={26} />
              </View>
          </View>
          <View style={styles.details}>
            <Text style={styles.topText}> You Account is Verified by Admin</Text>
            <Text style={styles.bottomText}> You can now post or bid </Text>
          </View>
          <View style={styles.arrow}>
            <Icon name="ios-arrow-forward" color='#009387' size={26} />
          </View>
        </View>
      </TouchableOpacity>
    );
};

export default NotiVerify;

const styles = StyleSheet.create({
  container: {
    marginTop: 4,
    height: 72,
    width: '97%',
    borderWidth: 1,
    flexDirection: 'row',
    borderColor: '#009387',
    alignItems: 'center', 
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderRadius: 4
  },

  icon: {
    width: '20%',
    justifyContent: 'center',
    alignItems: 'center'
  },

  logo: {
    height: 55,
    width: 55,
    backgroundColor: '#009387',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center'
  },

  pic: {
    height: 25,
    width: 25,
  },

  details: {
    height: '100%',
    width: '70%',
    justifyContent: 'center'
  },

  topText: {
    padding: 2,
    fontSize: 16
  },

  bottomText: {
    padding: 2,
    fontSize: 12,
    // textTransform: 'capitalize'
  },

  arrow: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center'
  },
});
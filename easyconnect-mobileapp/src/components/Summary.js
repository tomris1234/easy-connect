import React from 'react';
import { View, Text, Button, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';

const Summary = (props) => {
  const navigation = useNavigation();

    return (
      <View style={styles.container}>
        <View style={styles.sumHeader}>
          <Text style={styles.sumText}> Summary </Text>
          {props.from ? 
          <Icon name="md-create" color='#dcdcdc' size={25} style={{paddingRight: 5}}/>
          :
          null}
        </View>
        <View style={styles.sumBox}>
          <Text style={styles.sumAll}>Hello, Im a experienced driver, i have been driving for more than 5 years </Text>
        </View>
      </View>
    );
};

export default Summary;

const styles = StyleSheet.create({
  container: {
    marginTop: 4,
    marginBottom: 4,
    minHeight: 72,
    width: '97%',
    borderWidth: 1,
    borderColor: '#009387',
    alignItems: 'center', 
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderRadius: 4
  },

  sumHeader: {
    height: 40,
    width: '100%',
    backgroundColor: '#009387',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 6
  },

  sumText: {
    color: '#fff',
    fontSize: 18
  },

  sumBox: {
    marginTop: 3,
    paddingHorizontal: 5,
    minHeight: 60,
    width: '100%',
    backgroundColor: '#eff0f1',
    justifyContent: 'flex-start'
  },

  sumAll: {

  }
});
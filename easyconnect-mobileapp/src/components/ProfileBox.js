import React, {useState} from 'react';
import { View, Text, Button, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useNavigation } from '@react-navigation/native';
import profile from '../../assets/person.png';

const ProfileBox = (props) => {
  const navigation = useNavigation();
  const [count, setCount] = useState(3);

    return (
      <View style={styles.container}>
        <View style={styles.ImageBox}>
          <View style={styles.ImageCircle}>
            <Image source={profile} style={styles.pic}/>
          </View>
        </View>
        <View style={styles.star}>
          <Text style={styles.name}>
            {props.firstname} {props.lastname}
          </Text>
        </View>
        <View style={styles.email}>
          <Text  style={styles.emailAddress}> <Text  style={styles.emailTitle}></Text> {props.username} 
          </Text>
        </View>
        <View style={styles.all}>
          <TouchableOpacity style={styles.allButton} onPress={()=> navigation.navigate('OthersProfileScreen', {item: props.item})}>
            <Text style={styles.allText}> View Details </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
};

export default ProfileBox;

const styles = StyleSheet.create({
  container: {
    marginTop: 8,
    height: 270,
    width: '65%',
    borderWidth: 1,
    alignSelf: 'center',
    borderColor: '#009387',
    alignItems: 'center', 
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderRadius: 4
  },

  ImageBox: {
    height: '55%',
    width: '100%',
    justifyContent:'center',
    alignItems:'center'
  },

  ImageCircle: {
    marginTop: 7,
    height: 140,
    width: 140,
    borderColor: '#009387',
    borderWidth: 1,
    borderRadius: 70
  }, 

  pic: {
    height: '100%',
    width: '100%',
    borderRadius: 70
  },

  email: {
    // marginTop: 4,
    height: '10%',
    width: '100%',
    justifyContent:'center',
    alignItems:'center'
  },

  emailTitle: {
    paddingTop: 2,
    color: '#009387',
  },

  name: {
    paddingTop: 2,
    color:'black',
    textTransform: 'capitalize'
  },

  emailAddress: {
    paddingTop: 2,
    color:'black'
  },

  star:{
    height: '12%',
    width: '100%',
    justifyContent:'center',
    alignItems:'center'
  },

  all: {
    height: '23%',
    width: '100%',
    justifyContent:'center',
    alignItems:'center'
  },

  allButton: {
    height: '65%',
    width: '40%',
    justifyContent:'center',
    alignItems:'center',
    backgroundColor: '#009387',
    borderRadius: 5
  },

  allText: {
    color: '#fff'
  }

});
import React, {useState, useEffect} from 'react';
import { View, Text, Button, StyleSheet, StatusBar, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux'

const JobPost = (props) => {
  const navigation = useNavigation();
  const [dataProfile, setData] = useState('');
  const loginReducers = useSelector(state => state.loginReducers.data)

  useEffect(() => {
    makeRequest()
  }, []);

  const makeRequest = () => {
    const url = `https://ibm-challenge-api.herokuapp.com/api/v1/users/profile/${props.item.createdBy}`;
    fetch(url,
      {
        method: 'GET',
        headers: {  // these could be different for your API call
          'Content-Type': 'application/json',
          'token': loginReducers.token
        },
      })
      .then((response) => response.json())
      .then((json) => {
        if (json.data  && json.success === true) { // response success checking logic could differ
          setData(json.data)
        console.log(json, 'allll job dtas hahahahah eachhhhhhh name');
        }
      })
      .catch((err) => {
        console.log(err);
        setData(json.data)
      });
  };

    return (
      <TouchableOpacity style={styles.container} onPress={()=> navigation.navigate('JobsDetails',
       {item: props.item,
        firstname: dataProfile.firstname,
        lastname: dataProfile.lastname,
        key: props.item.id
        })}>
        <View style={styles.details}>
          <View style={styles.top}>
            <Text style={styles.topText}> {props.item.title} </Text>
          </View>
          <View style={styles.middle}>
            <Text > {props.item.description} </Text>
          </View>
          <View style={styles.bottom}>
            <View style={styles.last}>
              <Icons
                name="account-outline" 
                color='#009387'
                size={14}
                style={{paddingLeft: 4}}
                />
              <Text style={{textTransform: 'capitalize'}}> {dataProfile.firstname} {dataProfile.lastname}  </Text>
            </View>
            <View style={styles.last}>
              <Icons
                name="circle" 
                color='#009387'
                size={12}
                />
              <Text> Price: {props.item.minPrice}$-{props.item.maxPrice}$</Text>
            </View>
            <View style={styles.last}>
              <Icons
                name="circle" 
                color='#009387'
                size={12}
                />
              <Text> {props.item.bidders.length} bids </Text>
            </View>
          </View>
        </View>
        <View style={styles.arrow}>
          <Icon name="ios-arrow-forward" color='#009387' style={{marginRight:4}} size={26} />
        </View>
      </TouchableOpacity>
    );
};

export default JobPost;

const styles = StyleSheet.create({
  container: {
    marginTop: 5,
    minHeight: 100,
    maxHeight: 150,
    width: '97%',
    borderWidth: 1,
    flexDirection: 'row',
    borderColor: '#009387',
    alignItems: 'center', 
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderRadius: 5,
    alignSelf: 'center'
  },

  details: {
    width: '92%'
  },

  arrow: {
    width: '8%',
    // justifyContent: 'center',
    alignItems: 'center'
  },

  top: {
    marginLeft: 4,
    minHeight: 30,
    maxHeight: 50,
    justifyContent: 'center',
    paddingHorizontal: 5,
  },

  topText: {
    fontSize: 17,
    fontWeight: 'bold',
    textTransform: 'capitalize'
  },

  middle: {
    marginLeft: 4,
    minHeight: 40,
    maxHeight: 70,
    paddingHorizontal: 5,
    justifyContent: 'flex-start'
  },

  bottom: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 5,
  },

  last: {
    flexDirection: 'row',
    alignItems: 'center'
  }
});
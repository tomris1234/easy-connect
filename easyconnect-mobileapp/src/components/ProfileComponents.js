import React from 'react';
import { View, Text, Button, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';

const ProfileComponents = (props) => {
  const navigation = useNavigation();
    return (
      <View style={styles.container} >
        <View style={styles.icon}>
            <View style={styles.logo}>
                <Image source={props.icon} style={styles.pic}/>
            </View>
        </View>
        <View style={styles.details}>
            <Text style={styles.topText}>{props.title} </Text>
            <Text style={styles.bottomText}>{props.details} </Text>
        </View>
        <View style={styles.arrow}>
          {props.bool ?
          <Icon name="ios-arrow-down" color='#009387' size={26} />
          :
          <Icon name="ios-arrow-up" color='#009387' size={26} />
          }
        </View>
      </View>
    );
};

export default ProfileComponents;

const styles = StyleSheet.create({
  container: {
    marginBottom: 4,
    marginTop: 2,
    height: 80,
    width: '97%',
    borderWidth: 1,
    flexDirection: 'row',
    borderColor: '#009387',
    borderRadius: 8,
    alignItems: 'center', 
    justifyContent: 'center',
    backgroundColor: '#fff'
  },

  icon: {
    width: '20%',
    justifyContent: 'center',
    alignItems: 'center'
  },

  logo: {
    height: 45,
    width: 45,
    backgroundColor: '#009387',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center'
  },

  pic: {
    height: 25,
    width: 25,
  },

  details: {
    height: '100%',
    width: '70%',
    justifyContent: 'center'
  },

  topText: {
    padding: 2,
    fontSize: 16
  },

  bottomText: {
    padding: 2,
    fontSize: 12
  },

  arrow: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center'
  },
});
const INITIALL_STATE = {
    isLoading: false,
    isLoggedIn: false,
    isLogOut: false,
    data: '',
    isVerified: false,
    error: '',
    errormessage: ''
}

export default loginReducers = (state = INITIALL_STATE, action) => {
    switch(action.type){
        case 'LOADING_LOGIN':
            return{
                ...state,
                isLoading: true,
                isLoggedIn: false,
                isLogOut: true,
                data: '',
                isVerified: false,
                error: '',
                errormessage: ''
            }
        case 'SUCCESS_LOGIN':
            return{
                ...state,
                isLoading: false,
                isLoggedIn: true,
                isLogOut: false,
                data: action.data,
                isVerified: false,
                error: '',
                errormessage: ''
            }
        case 'SUCCESS_LOGOUT':
            return{
                ...state,
                isLoading: false,
                isLoggedIn: false,
                isLogOut: true,
            }
        case 'ERROR_LOGIN':
            return{
                isLoading: false,
                isLoggedIn: false,
                isLogOut: true,
                data: '',
                isVerified: false,
                error: action.error,
                errormessage: action.errormessage
            }
        default:
            return state;
    }
}

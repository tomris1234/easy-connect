// import AsyncStorage from '@react-native-community/async-storage';

export function isLoading(){
    return{
        type: 'LOADING_LOGIN',
        isLoading: true
    }
}

export function isLoggedIn(data){
    return{
        type: 'SUCCESS_LOGIN',
        isLoggedIn: true,
        data: data
    }
}

export function isLogOut(){
    return{
        type: 'SUCCESS_LOGOUT',
        isLogOut: true,
    }
}

export function isError(err, error){
    return{
        type: 'ERROR_LOGIN',
        error: err,
        errormessage: error
    }
}


export const login = (username, password) => {
    // const { username, password } = loginInput;
    console.log(username, password, 'nnnnnnnn');
    return (dispatch) => {  // don't forget to use dispatch here!
      dispatch(isLoading());
      return fetch('https://ibm-challenge-api.herokuapp.com/api/v1/users/login', {
        method: 'POST',
        headers: {  // these could be different for your API call
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "username": username,
            "password": password
        }),
      })
        .then((response) => response.json())
        .then((json) => {
          console.log(json);

          if (json.success === true) { // response success checking logic could differ
            dispatch(isLoggedIn(json.data)); // our action is called here
            // AsyncStorage.setItem("token", json.token)
            // console.log(json.data);
          } else {
            dispatch(isError('Login Failed', 'Some error occured, please retry again')); // our action is called 
          }
        })
        .catch((err) => {
          dispatch(isError('Login Failed', 'Some error occured, please retry')); // our action is called 
          console.log(err);
        });
    };
  };

export function isLoading(){
  return{
      type: 'LOADING_REGISTER',
      isLoading: true,
  }
}

export function isRegister(data){
  return{
      type: 'SUCCESS_REGISTER',
      isRegister: true,
      data: data
  }
}

export function isError(err, error){
  return{
      type: 'ERROR_REGISTER',
      error: err,
      errormessage: error
  }
}

export function reset(){
  return{
      type: 'RESET_REGISTER',
  }
}


export const register = (data) => {
  console.log(data, 'nnnnnnnn');
  return (dispatch) => {  // don't forget to use dispatch here!
    dispatch(isLoading());
    return fetch('https://ibm-challenge-api.herokuapp.com/api/v1/users', {
      method: 'POST',
      headers: {  // these could be different for your API call
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "username": data.username,
        "password": data.password,
        "firstname": data.fullname,
        "lastname": data.lastname,
        "phone": data.phone,
        "role": data.role
      }),
    })
      .then((response) => response.json())
      .then((json) => {
        console.log(json, 'registerrrrrrrrr');
        if (json.success === true) { // response success checking logic could differ
          dispatch(isRegister(json)); // our action is called here
        } else {
          dispatch(isError('Register Failed', 'Some error occured, please retry')); // our action is called 
        }
      })
      .catch((err) => {
        dispatch(isError('Register Failed', err)); // our action is called 
        console.log(err,  'errrrrr');
      });
  };
};

const INITIALL_STATE = {
    isLoading: false,
    isRegister: false,
    data: '',
    error: '',
    errormessage: ''
}

export default registerReducers = (state = INITIALL_STATE, action) => {
    switch(action.type){
        case 'LOADING_REGISTER':
            return{
                ...state,
                isLoading: true,
            }
        case 'RESET_REGISTER':
            return{
                isLoading: false,
                isRegister: false,
                data: '',
                error: '',
                errormessage: ''
            }
        case 'SUCCESS_REGISTER':
            return{
                ...state,
                isLoading: false,
                isRegister: true,
                data: action.data,
                error: '',
                errormessage: ''
            }
        case 'ERROR_REGISTER':
            return{
                isLoading: false,
                isRegister: false,
                data: '',
                error: action.error,
                errormessage: action.errormessage
            }
        default:
            return state;
    }
}

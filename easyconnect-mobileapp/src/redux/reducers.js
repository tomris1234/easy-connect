import { combineReducers } from 'redux';
import loginReducers from './user/reducers';
import registerReducers from './register/reducers';

const rootReducer = combineReducers({
    loginReducers,
    registerReducers,
});

export default rootReducer;
import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import RootStackScreen from './src/navigator/RootStackScreen';
import ConnectScreen from './src/navigator/ConnectScreen';
import { useSelector, useDispatch } from 'react-redux'

const RootStack = createStackNavigator();
const DStack = createStackNavigator();

export default function Main() {
    const loginReducers = useSelector(state => state.loginReducers)

  return (
      <NavigationContainer>
        <RootStack.Navigator screenOptions={{headerShown:false}} >
           {loginReducers.isLoggedIn ? 
           <RootStack.Screen name="ConnectScreen" component={DStackScreen} /> :
           <RootStack.Screen name="RootStackScreen" component={RootStackScreen}/>}
        </RootStack.Navigator>
      </NavigationContainer>
  );
}

const DStackScreen = ({navigation}) => (
  <DStack.Navigator >
    <DStack.Screen name="DStack" component={ConnectScreen} options={{headerShown: false}}/>
  </DStack.Navigator>
  );
import base64 from 'react-native-base64'
// import { USERNAME, PASSWORD, URL, SKILL_ID } from 'react-native-dotenv'

// Watson Assistant API documentation:
// https://console.bluemix.net/apidocs/assistant
const MessageRequest = (input) => {
    let body = {
        alternate_intents: true,
        input: {
            'text': input
        }
    };
    console.log('input:',input);
    return fetch('https://api.au-syd.assistant.watson.cloud.ibm.com/instances/2a30885f-f4f5-45cb-b838-e8afe4954fda/v1/workspaces/7285d8a8-7a91-4a32-b3a9-fcdcfcbd332c/message?version=2018-09-20', {
        method: 'POST',
        headers: {
            Authorization: 'Basic ' + base64.encode("apikey" + ":" + "n8Voe8LQDKTs0cWvOOr5uuEP3-fkEaQdHM6yl7IrCst3"),
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
    })
        .then((response) => response.json())
        .then((responseJson) => {

            // console.log('dasdas',responseJson);
            return responseJson;
        })
        .catch((error) => {
            console.error('res',error);
        });

}

export default MessageRequest;
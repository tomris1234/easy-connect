const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const mongoose = require("mongoose");
const helmet = require("helmet");
const createError = require("http-errors");
require("dotenv").config({ path: "variables.env" });

const RouteManager = require("./routes");

mongoose.connect(process.env.DB_URL);

mongoose.connection.on("connected", function () {
  console.log("Connected to database successfully.");
});

mongoose.connection.on("error", function (err) {
  console.log("Database error:" + " " + err);
});

const app = express();

app.use(morgan("combined"));
app.use(helmet());

const port = Number(process.env.PORT || 7777);

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());

// Add headers
app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type,authorization"
  );
  next();
});



// ROUTES FOR OUR API
app.use("/", RouteManager);
app.use(express.static(path.join(__dirname, "public")));
app.use('/uploads', express.static(path.join(__dirname, '/uploads')));

app.use(function (req, res, next) {
  next(createError(404));
});
app.use(function (error, req, res, next) {
  console.error(error)
  res.status(error.status || 500).json(error);
});

app.listen(port, function () {
  console.log("Server running at port:" + port);
});

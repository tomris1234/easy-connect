const TextUtils = {
  pascalToCamelCase: (s) =>
    s.replace(/\.?([A-Z])/g, (x, y) => "_" + y.toLowerCase()).replace(/^_/, ""),
  escapeRegex: (text) => text.replace(/[-[\]{}()*=?.,\\^$|#\s]/g, "\\$&"),
};

const countDaysInDate = (f_date, s_date) => {
  let oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
  if (!f_date) f_date = new Date();
  if (!s_date) s_date = new Date();

  let firstDate = new Date(f_date);
  let secondDate = new Date(f_date);
  let days_count = Math.round(
    Math.abs((firstDate.getTime() - secondDate.getTime()) / oneDay)
  );
  return days_count;
};

const Response = {
  success: ({ msg = "N/A", status = 200, data }) => {
    return {
      success: true,
      status: status || 200,
      message: msg,
      data: data || null,
    };
  },
  fail: ({ msg, status = 500 }) => {
    return {
      success: false,
      status: status,
      message: msg,
    };
  },
};

const DataUtils = {
  paging: async ({ offset = 0, limit = 10, sort, model, query }) => {
    query.push({
      $sort: sort,
    });
    query.push({
      $facet: {
        data: [
          {
            $skip: parseInt(offset),
          },
          {
            $limit: parseInt(limit),
          },
        ],
        total: [
          {
            $group: {
              _id: null,
              count: {
                $sum: 1,
              },
            },
          },
        ],
      },
    });
    let matchedData = await model.aggregate(query);

    let data = [],
      total = 0;
    if (matchedData[0].data.length > 0) {
      data = matchedData[0].data;
      total = matchedData[0].total[0].count;
    }

    return {
      data,
      total,
      limit,
      totalPages: Math.ceil(total / limit),
      page: Math.round(offset / limit) + 1,
    };
  },
};
module.exports = {
  Response,
  DataUtils,
  TextUtils,
  countDaysInDate,
};

class CustomError extends Error {
  constructor(message, name, httpCode) {
    super();
    this.message = message;
    this.data = {
      group: process.env.APP_NAME,
      type: "customerror",
      message: message,
      name: name || "none",
      httpCode: httpCode || 500,
    };
    this.status = httpCode || 500;
    this.className = this.constructor.name;
    this.stack = new Error(message).stack;
  }
}

const ERR = {
  DEFAULT: new CustomError("Error Occured", "none", 500),
  INVALID_TOKEN: new CustomError("Token is invalid", "invalid_token", 500),
  AUTH_FAIL: new CustomError(
    "Authentication failed. Please try again.",
    "auth_fail",
    401
  ),
  CM_EXISTS: new CustomError(
    "Cross Match for this patient already exists.",
    "cm_exists",
    400
  ),
  UNAUTHORIZED: new CustomError(
    "You don't have access to do that.",
    "unauthorized",
    401
  ),
  PWD_SAME: new CustomError(
    "Please send different new password",
    "pwd_same",
    400
  ),
  PWD_NOTMATCH: new CustomError(
    "Old password does not match.",
    "pwd_notmatch",
    400
  ),
  TOKEN_REQ: new CustomError("Must send access_token", "token_req", 400),
  USER_NOEXISTS: new CustomError("User does not exists.", "user_noexists", 400),
  //DEFAULT: new CustomError('', '', 400),
};

const throwError = (err) => {
  throw err;
};
module.exports = { Error: CustomError, ERR, throwError };

const jwt = require("jsonwebtoken");
const UserController = require("../controllers/user.controller");
const SECRET_KEY = process.env.APP_SECRET;
const { ERR } = require("./error");
const SecureAPI = (roles) => {
  let role_arr = roles ? roles.split(",") : [];
  return function (req, res, next) {
    let token =
      req.headers.authorization ||
      req.cookies.token ||
      req.body.token ||
      req.query.token ||
      req.headers["token"];
    if (!token) {
      throw ERR.TOKEN_REQ;
    }
    jwt.verify(token, SECRET_KEY, function (err, token_data) {
      if (err) {
        throw ERR.INVALID_TOKEN;
      } else {
        if (token_data) {
          UserController.get({ _id: token_data.id })
            .then((user) => {
              if (user && user.data) {
                let role_assigned = user.data.role;
                let _access = role_arr.includes(role_assigned);
                if (role_arr.length < 1) {
                  req.token_data = user.data;
                  return next();
                }
                if (_access === true) {
                  req.token_data = user.data;
                  return next();
                } else {
                  throw ERR.UNAUTHORIZED;
                }
              } else {
                throw ERR.USER_NOEXISTS;
              }
            })
            .catch((err) => next(err));
        }
      }
    });
  };
};
module.exports = { SecureAPI };

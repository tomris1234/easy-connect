const nodemailer = require("nodemailer");
const handlebars = require("handlebars");
const fs = require("fs");
const path = require("path");
const app_url = process.env.APP_URL;
const USERNAME = process.env.GMAIL_USER;
const PASSWORD = process.env.GMAIL_PASS;
handlebars.registerHelper("app_url", () => app_url);
let filePath;
const send = ({ action = "fogot_password", send_to, subject, data }) => {
  new Promise((resolve, reject) => {
    if (action == "signup") {
      filePath = "../../public/email/signup.hbs";
    }

    if (action == "forgot_password") {
      filePath = "../../public/email/forgotPassword.hbs";
    }
    
    if (action == "verified") {
      filePath = "../../public/email/verified.hbs";
    }

    if (action == "bid") {
      filePath = "../../public/email/bid.hbs";
    }
    const readHTMLFile = (path, callback) => {
      fs.readFile(path, { encoding: "utf-8" }, (err, html) => {
        if (err) {
          callback(err);
        } else {
          callback(null, html);
        }
      });
    };
    const transporter = nodemailer.createTransport({
      service: "gmail",
      port: 587,
      secure: false,
      auth: {
        user: USERNAME,
        pass: PASSWORD,
      },
       tls: {
          rejectUnauthorized: false
      }
    });

    readHTMLFile(path.join(__dirname + filePath), (err, html) => {
      var template = handlebars.compile(html);
      var htmlToSend = template(data);
      var mailOptions = {
        from: USERNAME,
        to: `${send_to}`,
        subject: subject || "No subject sent!",
        html: htmlToSend,
      };
      transporter.sendMail(mailOptions, (error, response) => {
        if (error) {
          reject(error);
        }
        resolve(response);
      });
      transporter.close();
    });
  });
};

module.exports = { send };

const RatingModel = require('../models/rating');
const { Response } = require("../utils");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

const create = async (payload, user) => {
    try {
        let model = new RatingModel(payload);
        model.ratingBy = user._id;
        let data = await model.save();
        return Response.success({
            msg: "Rating Successfully Added",
            status: 200,
            data
        })
    }
    catch (e) {
        return Response.fail({ msg: e });
    }
};

const update = async (payload, user) => {
    try {
        let updateData = await RatingModel.findById(payload.id);
        if (updateData) {
            updateData.rating = payload.rating;
            updateData.note = payload.note;
            let data = await updateData.save();
            return Response.success({
                msg: "Rating Successfully Updated",
                status: 200,
                data
            })
        } else {
            return Response.fail({ msg: 'No Data Found' });
        }
    }
    catch (e) {
        return Response.fail({ msg: e });
    }
};

const remove = async (id) => {
    try {
        let data = await RatingModel.findByIdAndRemove(id);
        return Response.success({
            msg: "Rating Successfully Deleted",
            status: 200,
            data: true
        })
    } catch (e) {
        return Response.fail({ msg: e });
    }
};

const getOne = async (id) => {
    try {
        let data = await RatingModel.findById(id);
        return Response.success({
            msg: "One Rating Successfully fetched",
            status: 200,
            data
        })
    }
    catch (e) {
        return Response.fail({ msg: e });
    }
};

const getAll = async () => {
    try {
        let data = await RatingModel.find();
        return Response.success({
            msg: "Rating Successfully Added",
            status: 200,
            data
        })
    }
    catch (e) {
        return Response.fail({ msg: e });
    }
};

const getRate = async (id) => {
    try {
        let matchObj = { "user": ObjectId(id) }
        let data = await RatingModel.aggregate([
            { $match: matchObj },
            {
                $group: {
                    "_id": null,
                    "rate": {
                        $avg: "$rating"
                    }
                }
            }
        ]);
        return data[0];
    }
    catch (e) {
        throw (e);
    }
}

const getAverageRatingByUserId = async (id) => {
    try {
        let data = await getRate(id);
        return Response.success({
            msg: "Average Rating Successfully Fetched",
            status: 200,
            data: { rating: data ? data.rate : 0 }
        })
    }
    catch (e) {
        return Response.fail({ msg: e });
    }
};


module.exports = {
    create,
    update,
    remove,
    getOne,
    getAll,
    getRate,
    getAverageRatingByUserId
};

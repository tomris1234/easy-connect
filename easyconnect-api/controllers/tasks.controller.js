const TaskModel = require('../models/tasks');
const JobModel = require('../models/jobs');
const userModel=require('../models/users');
const { Response } = require("../utils");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

const create = async (payload, user) => {
    try {
        let job = await JobModel.findById(payload.job);
        let disableUser = await userModel.findOne({'_id':ObjectId(job.createdBy)});
        let task = await TaskModel.findOne({ student: user._id, job: payload.job });
        if (!task) {
            if (job) {
                if (!job.bidders.includes(user._id)) { job.bidders.push(user._id); }
                const model = new TaskModel(payload);
                model.lastUpdatedBy = user._id;
                model.student = user._id;
                const data = await model.save();
                const data1 = await JobModel.findOneAndUpdate({ "_id": payload.job }, job, { new: true });
                let email_payload = {
                    name: disableUser.firstname+' '+disableUser.lastname,
                    bidder: user.firstname+' '+user.lastname
                  };
                //   await mailer.send({
                //     action: "bid",
                //     send_to: disableUser.username,
                //     subject: "Bidding",
                //     data: email_payload,
                //   });
                return Response.success({
                    msg: "Task sucessfully created",
                    status: 200,
                    data: data
                });
            } else {
                return Response.fail({ msg: 'Jobs Not found' });
            }
        } else {
            return Response.fail({ msg: 'User Already Place Bid' });
        }

    } catch (e) {
        return Response.fail({ msg: e });
    }
};

const update = async (payload, user) => {
    try {
        const taskData = await TaskModel.findById(payload.id);
        if (taskData) {
            taskData.job = payload.job;
            taskData.status = payload.status;
            taskData.amount = payload.amount;
            taskData.student = payload.student;
            taskData.guest = payload.guest;
            taskData.specialNote = payload.specialNote;
            taskData.lastUpdatedBy = user._id;
            taskData.lastUpdatedAt = new Date();
            let data = await taskData.save();
            return Response.success({
                msg: "Task sucessfully created",
                status: 200,
                data
            });
        }
        else {
            return Response.fail({ msg: "No Data Found To Update" });
        }
    } catch (e) {
        return Response.fail({ msg: e });
    }
};

const remove = async (id) => {
    try {
        const data = await TaskModel.findByIdAndRemove(id);
        return Response.success({
            msg: "Task sucessfully removed",
            status: 200,
            data: true
        });
    } catch (e) {
        return Response.fail({ msg: e });
    }
};

const getOne = async (id) => {
    try {
        const data = await TaskModel.findById(id);
        return Response.success({
            msg: "Task sucessfully fetched",
            status: 200,
            data: data
        });
    } catch (e) {
        return Response.fail({ msg: e });
    }
};

const getAll = async (payload, user) => {
    try {
        const data = await TaskModel.find();
        return Response.success({
            msg: "Tasks sucessfully fetched",
            status: 200,
            data: data
        });
    } catch (e) {
        return Response.fail({ msg: e });
    }
};

const getByJobId = async (job) => {
    try {
        const data = await TaskModel.find({ job});
        return Response.success({
            msg: "Tasks sucessfully fetched",
            status: 200,
            data: data
        });
    } catch (e) {
        return Response.fail({ msg: e });
    }
}

const getbyguestId = async(guest)=>{
    try {
        const data = await TaskModel.find({ guest,status:'Started'  });
        return Response.success({
            msg: "Tasks sucessfully fetched",
            status: 200,
            data: data
        });
    } catch (e) {
        return Response.fail({ msg: e });
    }
}

module.exports = {
    create,
    update,
    remove,
    getOne,
    getAll,
    getByJobId,
    getbyguestId
};

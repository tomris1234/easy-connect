const JobModel = require("../models/jobs");
const { Response } = require("../utils");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const UserModel=require('../models/users');
const mailer = require("../utils/mailer");

const createJob = async (payload, user) => {
    try {
        const model = new JobModel(payload);
        model.createdBy = user._id;
        const data = await model.save();
        return Response.success({
            msg: "Job sucessfully created",
            status: 200,
            data: data
        });
    } catch (e) {
        return Response.fail({ msg: e });
    }
};

const getAllJob = async (authData) => {
    try {
        const data = await JobModel.find({ createdBy: ObjectId(authData._id) });
        return Response.success({
            msg: "Job Sucessfully Fetched",
            status: 200,
            data
        });
    } catch (e) {
        return Response.fail({ msg: e });
    }
};

const getAllTotalJob = async () => {
    try {
        const data = await JobModel.find();
        return Response.success({
            msg: "Job Sucessfully Fetched",
            status: 200,
            data
        });
    } catch (e) {
        return Response.fail({ msg: e });
    }
}

const getOneJobByJobId = async id => {
    try {
        const data = await JobModel.findById(id);
        return Response.success({
            msg: "Job Sucessfully Fetched",
            status: 200,
            data
        });
    } catch (e) {
        return Response.fail({ msg: e });
    }
};

const updateJobById = async (id, payload, user) => {
    payload.updatedBy = user._id;
    try {
        const updatedData = await JobModel.findByIdAndUpdate(
            id,
            { $set: payload },
            {
                new: true
            }
        );
        return Response.success({
            msg: "Job Sucessfully Updated",
            status: 200,
            data: updatedData
        });
    } catch (e) {
        return Response.fail({ msg: e });
    }
};

const deleteJob = async id => {
    try {
        const data = await JobModel.findByIdAndDelete(id);
        if (data) {
            return Response.success({
                msg: "Job Successfully Deleted",
                status: 200,
                data: true
            });
        } else {
            return Response.fail({
                msg: "No Job Found With This Job Id",
                status: 200
            });
        }
    } catch (e) {
        return Response.fail({ msg: e });
    }
};

const lockJobForOneUser=async(payload,authData)=>{
let job=await JobModel.findById(payload.jobId);
let user=await UserModel.findById(payload.userId);
if(job){
if(job.bidders.includes(payload.userId)){
    job.status='Locked';
    job.doingBy=payload.userId;
   let data=await job.save();
   let email_payload = {
    name: user.firstname+' '+user.lastname,
    owner:authData.firstname+' '+authData.lastname
  };

//   await mailer.send({
//     action: "task_accepted",
//     send_to: user.username,
//     subject: "Your Bid Has Been Accepted",
//     data: email_payload,
//   });
  return Response.success({
    msg: "User Successfully Accepted",
    status: 200,
    data: job
});
}else{
    return Response.fail({ msg: "Given User In Not A Bidder" }); 
}
}else{
    return Response.fail({ msg: "No Job Found" }); 
}
} 

const changeJobStatus=async(payload)=>{
    let job=await JobModel.findById(payload.jobId);
    if(job){
        job.status=payload.status;
       let data=await job.save();
      return Response.success({
        msg: "User Successfully Updated",
        status: 200,
        data: data
    });
    }else{
        return Response.fail({ msg: "No Job Found" }); 
    }
    } 

module.exports = {
    createJob,
    getOneJobByJobId,
    getAllJob,
    updateJobById,
     getAllTotalJob,
    deleteJob,
    lockJobForOneUser,
    changeJobStatus
};

const VerificationModel = require("../models/verification");
const UserModel = require("../models/users");
const { Response } = require("../utils");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

// const create = async (payload, user) => {
//     try {
//         const model = new VerificationModel(payload);
//         model.createdBy = user._id;
//         const data = await model.save();
//         return Response.success({
//             msg: "Verification initiated sucessfully.",
//             status: 200,
//             data: data
//         });
//     } catch (e) {
//         return Response.fail({ msg: e });
//     }
// };

const create = async (payload, user, file) => {
    try {
        console.error(user, payload)

        let document = await VerificationModel.findOne({ requestedBy: user._id });
        if (document) {
            return Response.fail({ msg: "Verification Document Already Exist" });
        }
        else {
            if (user.role == payload.documentType) {
                const model = new VerificationModel(payload);
                model.document.fileUrl = file.path;
                model.document.Id = payload.Id;
                model.requestedBy = user._id;
                const data = await model.save();
                return Response.success({
                    msg: "Verification initiated sucessfully and is in Pending",
                    status: 200,
                    data: data
                });
            } else {
                return Response.fail({ msg: "Respective User Can Only add their document" });
            }

        }
    } catch (e) {
        return Response.fail({ msg: e });
    }
};

const update = async (payload, user, file) => {
    try {
        let document = await VerificationModel.findOne({ requestedBy: user._id });
        if (document) {
            model.document.fileUrl = file.path;
            model.document.Id = payload.Id;
            document.status = 'Pending';
            const data = await document.save();
            return Response.success({
                msg: "Verification initiated sucessfully and is in Pending",
                status: 200,
                data: data
            });
        }
        else {
            return Response.fail({ msg: "Document not Found" });
        }
    } catch (e) {
        return Response.fail({ msg: e });
    }
};

const veriy = async (payload, authData) => {
    try {
        let user = await UserModel.findById(payload.userId);
        let document = await VerificationModel.findOne({ requestedBy: payload.userId });
        if (authData.role == 'Admin') {
            if (document) {
                document.status = payload.status
                let updatedDoc = await document.save();
                let updateModel = { isVerified: payload.status }
                if (payload.status == 'Rejected') {
                    updateModel = { isVerified: payload.status, isActive: false }
                }
                let updateUserStatus = await UserModel.findByIdAndUpdate(payload.userId, updateModel, { new: true })
                let email_payload = {
                    name: user.firstname+' '+user.lastname,
                    status: payload.status
                  };
                  await mailer.send({
                    action: "verified",
                    send_to: user.username,
                    subject: "Verification",
                    data: email_payload,
                  });
                return Response.success({ msg: "User status verified", updatedDoc });
            } else {
                return Response.fail({ msg: "Veriication Document Not Found" });
            }
        }
        else {
            return Response.fail({ msg: "Only Admin Can Verify User" });
        }
    }
    catch (e) {
        return Response.fail({ msg: e });
    }
}

const getOne = async (id) => {
    try {
        let data = await VerificationModel.findById(id);
        return Response.success({ msg: "", data });
    }
    catch (e) {
        return Response.fail({ msg: e });
    }
}

const getAllforstudent = async () => {
    try {
        let data = await VerificationModel.find({ documentType: 'Student' });
        return Response.success({ msg: "", data });
    }
    catch (e) {
        return Response.fail({ msg: e });
    }
}

const getAllforguest = async () => {
    try {
        let data = await VerificationModel.find({ documentType: 'Guest' });
        return Response.success({ msg: "", data });
    }
    catch (e) {
        return Response.fail({ msg: e });
    }
}

const deleteDocument = async (authData, id) => {
    try {
        let user = await UserModel.findById(authData._id);
        if (user.role == 'Admin') {
            let document = await VerificationModel.findOneAndDelete({ requestedBy: ObjectId(id) });
            return Response.success({ msg: "Document Deleted Successfully", data: true });
        }
        else {
            return Response.fail({ msg: "Only Admin Can delete documents" });
        }
    }
    catch (e) {
        return Response.fail({ msg: e });
    }
}
const getByOneUser = async (id) => {
    try {
        let data = await VerificationModel.find({ requestedBy: ObjectId(id) });
        return Response.success({ msg: "", data });
    }
    catch (e) {
        return Response.fail({ msg: e });
    }
}
module.exports = {
    create,
    veriy,
    update,
    getOne,
    getAllforstudent,
    getAllforguest,
    getByOneUser,
    deleteDocument
};

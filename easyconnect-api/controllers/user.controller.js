const UserModel = require("../models/users");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const RatingController = require('../controllers/rating.controller');
const { Response, DataUtils, countDaysInDate } = require("../utils");
const secret_key = process.env.APP_SECRET;
const _salt = parseInt(process.env.SALT_ROUNDS);
const mailer = require("../utils/mailer");
var path = require('path');
const fs = require('fs');

const signup = async (payload) => {
  try {
    if (payload.password.length < 6)
      return Response.fail({ msg: "Password must be at least 6 characater." });
    const salt = bcrypt.genSaltSync(_salt);
    const hash = bcrypt.hashSync(payload.password, salt);
    delete payload.password;
    payload.password = hash;
    let obj = UserModel(payload);
    let doc = await obj.save();
    let email_payload = {
      name: payload.firstname+' '+payload.lastname
    };
    await mailer.send({
      action: "signup",
      send_to: payload.username,
      subject: "New User Created",
      data: email_payload,
    });
    return Response.success({ msg: "User account created.", data: doc });
  } catch (e) {
    return Response.fail({ msg: e });
  }
};

const auth = async (pass, hashedPass) => {
  const hash = hashedPass;
  const match = await bcrypt.compare(pass, hash);
  return match;
};

const changePassword = async (userId, payload) => {
  const { newPassword } = payload;
  const { oldPassword } = payload;
  try {
    let user = await get(userId);
    if (!user) return Response.fail({ msg: "User does not exist." });
    const match = await bcrypt.compare(oldPassword, user.data.password);
    if (match !== true)
      return Response.fail({ msg: "Old password did not match." });
    let doc = await resetPassword(user.data._id, newPassword);
    return Response.success({
      msg: "Password updated successfully.",
      data: doc,
    });
  } catch (e) {
    return Response.fail({ msg: e });
  }
};

const resetPassword = async (userId, rawPassword) => {
  const salt = bcrypt.genSaltSync(_salt);
  const newHash = bcrypt.hashSync(rawPassword, salt);
  return UserModel.findOneAndUpdate(
    { _id: userId },
    { $set: { password: newHash } },
    { new: true }
  );
};

const login = async (payload) => {
  try {
    let currentDate = new Date();
    const user = await UserModel.findOne({ username: payload.username });
    if (user.isVerified == 'Rejected') {
      return Response.fail({ msg: "User is Rejected. Please Contact With Admin" });
    }
    if (!user) {
      return Response.fail({ msg: "User does not exist." });
    }
    const match = await auth(payload.password, user.password);
    if (match === false) {
      return Response.fail({ msg: "Password did not match." });
    }
    let _expire = new Date(currentDate.getTime() + 24 * 60 * 60 * 1000);
    const _token = jwt.sign(user.toJSON(), secret_key, {
      expiresIn: 60 * 60 * 24 * 365, // 365 days
    });
    return Response.success({
      msg: "Logged in successfully.",
      data: { data: user, token: _token, expiresIn: _expire.getTime() },
    });
  } catch (e) {
    return Response.fail({ msg: e });
  }
};

const forgotPassword = async (email) => {
  try {
    let user = await UserModel.findOne({ username: email });
    if (user.isActive == 'Rejected') {
      return Response.fail({ msg: "User is Rejected. Please Contact With Admin" });
    }
    if (!user) {
      return Response.fail({ msg: "User does not exist." });
    }
    let _token = Math.floor(100000 + Math.random() * 900000);
    let exp_date = new Date();
    exp_date.setDate(exp_date.getDate() + 1);

    let payload = {
      "token.purpose": "forgot_password",
      "token.data": _token,
      "token.expiry_date": exp_date,
    };

    let email_payload = {
      name: user.firstname,
      token: _token,
    };

    await update(user._id, payload);
    await mailer.send({
      action: "forgot_password",
      send_to: email,
      subject: "Recover Password!",
      data: email_payload,
    });
    return Response.success({ msg: "Email sent to reset password" });
  } catch (e) {
    return Response.fail({ msg: e });
  }
};

const recoverPassword = async ({ token, newPassword }) => {
  try {
    let user = await UserModel.findOne({ "token.data": token });
    if (user.isVerified == 'Rejected') {
      return Response.fail({ msg: "User is Rejected. Please Contact With Admin" });
    }
    if (!user) {
      return Response.fail({ msg: "Token is invalid." });
    }
    let f_date = user.token.expiry_date;
    let s_date = new Date();
    let days = countDaysInDate(f_date, s_date);
    if (days !== 0) {
      return Response.fail({
        msg: "Token is expired, Please make a request again.",
      });
    }
    await resetPassword(user._id, newPassword);
    return Response.success({ msg: "Password reset successfull." });
  } catch (e) {
    return Response.fail({ msg: e });
  }
};

const listAll = async ({ page, limit, search }) => {
  try {
    let offset = (page - 1) * limit;
    let query = {};
    if (search) {
      query = { name: { $regex: search, $options: "i" } };
    }
    let data = await UserModel.find({ isActive: true });
    // let data = await DataUtils.paging({
    //   offset,
    //   limit,
    //   sort: { created_at: -1 },
    //   model: UserModel,
    //   query: [
    //     {
    //       $match: query,
    //     },
    //   ],
    // });
    return Response.success({ data: data });
  } catch (e) {
    return Response.fail({ msg: e });
  }
};

const listDisablePeople = async () => {
  try {
    let data = await UserModel.find({ role: 'Guest', isActive: true });
    return Response.success({ data: data });
  } catch (e) {
    return Response.fail({ msg: e });
  }
}

const listStudents = async () => {
  try {
    let data = await UserModel.find({ role: 'Student', isActive: true });
    return Response.success({ data: data });
  } catch (e) {
    return Response.fail({ msg: e });
  }
}

const get = async (userId) => {
  var user = await UserModel.findById(userId);
  return Response.success({ data: user });
};

const update = async (userId, payload) => {
  try {
    let user = await UserModel.findByIdAndUpdate(
      userId,
      { $set: payload },
      { new: true }
    );
    return Response.success({ msg: "User updated successfully.", data: user });
  } catch (e) {
    return Response.fail({ msg: e });
  }
};

const remove = async (userId) => {
  try {
    await UserModel.findByIdAndDelete(userId);
    return Response.success({ msg: "User account deleted." });
  } catch (e) {
    return Response.fail({ msg: e });
  }
};

// const verify = async (payload,authData) => {
//   try {
//     let user = await UserModel.findById(payload.userId);
//     if (user) {
//       if (authData.role == 'Admin') {
//         user.isVerified = payload.isVerified;
//         let data = await user.save();
//         return Response.success({ msg: "User status verified", data });
//       }
//       else {
//         return Response.fail({ msg: "Only Admin Can Verify User" });
//       }
//     }
//     else {
//       return Response.fail({ msg: "user Not Found" });
//     }
//   } catch (e) {
//     return Response.fail({ msg: e });
//   }
// }

const updateProfilePic = async (file, authData) => {
  try {
    let user = await UserModel.findById(authData._id);
    if (user) {
      let dirname = path.join(__dirname + '../../' + user.profilePic);
      if (fs.existsSync(dirname)) {
        fs.unlinkSync(dirname);
      }
      user.profilePic = file.path;
      let data = await user.save();
      return Response.success({ msg: "Profile Picture Updated Successfully", data });
    }
    else {
      return Response.fail({ msg: "User Not Found" });
    }
  } catch (e) {
    return Response.fail({ msg: e });
  }
}
const adminlogin = async (payload) => {
  try {
    let currentDate = new Date();
    const user = await UserModel.findOne({ username: payload.username });
    if (!user) {
      return Response.fail({ msg: "User does not exist." });
    }
    if (user.role != 'Admin') {
      return Response.fail({ msg: "Only Admin Can Login " });
    }
    const match = await auth(payload.password, user.password);
    if (match === false) {
      return Response.fail({ msg: "Password did not match." });
    }
    let _expire = new Date(currentDate.getTime() + 24 * 60 * 60 * 1000);
    const _token = jwt.sign(user.toJSON(), secret_key, {
      expiresIn: 60 * 60 * 24 * 365, // 365 days
    });
    return Response.success({
      msg: "Logged in successfully.",
      data: { data: user, token: _token, expiresIn: _expire.getTime() },
    });
  } catch (e) {
    return Response.fail({ msg: e });
  }
};

const listAllNotVerified = async () => {
  try {
    let data = await UserModel.find({ isActive: true, isVerified: 'Pending' });
    return Response.success({ data: data });
  } catch (e) {
    return Response.fail({ msg: e });
  }
}
module.exports = {
  recoverPassword,
  forgotPassword,
  changePassword,
  resetPassword,
  remove,
  listAll,
  login,
  signup,
  get,
  update,
  listDisablePeople,
  listStudents,
  updateProfilePic,
  // verify,
  listAllNotVerified,
  adminlogin
};

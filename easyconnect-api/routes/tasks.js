const express = require("express");
const router = express.Router();
const TaskController = require('../controllers/tasks.controller');
const { SecureAPI } = require("../utils/secure");

router.post('/', SecureAPI(), (req, res, next) => {
    TaskController.create(req.body, req.token_data)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
})

router.get('/all', SecureAPI(), (req, res, next) => {
    TaskController.getAll()
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
})
router.get('/:id', SecureAPI(), (req, res, next) => {
    TaskController.getOne(req.params.id)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
})
router.get('/job/:id', SecureAPI(), (req, res, next) => {
    TaskController.getByJobId(req.params.id)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
})

router.get('/guest/:id', SecureAPI(), (req, res, next) => {
    TaskController.getbyguestId(req.params.id)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
})


router.put('/', SecureAPI(), (req, res, next) => {
    TaskController.update(req.body, req.token_data)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
})
router.delete('/:id', SecureAPI(), (req, res, next) => {
    TaskController.remove(req.params.id)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
})

module.exports = router;

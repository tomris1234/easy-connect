const express = require("express");
const router = express.Router();
const JobController = require("../controllers/job.controller");
const { SecureAPI } = require("../utils/secure");
const secure = require("../utils/secure");

router.post("/", SecureAPI(), (req, res, next) => {
    JobController.createJob(req.body, req.token_data)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
});

router.post("/lockJoborUser", SecureAPI(), (req, res, next) => {
    JobController.lockJobForOneUser(req.body, req.token_data)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
});

router.post("/changeJobStatus", SecureAPI(), (req, res, next) => {
    JobController.changeJobStatus(req.body)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
});

router.get("/", SecureAPI(), (req, res, next) => {
    JobController.getAllJob(req.token_data)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
});

router.get("/all", SecureAPI(), (req, res, next) => {
    JobController.getAllTotalJob()
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
});

router.get("/:id", SecureAPI(), (req, res, next) => {
    JobController.getOneJobByJobId(req.params.id)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
});

router.put("/:id", SecureAPI(), (req, res, next) => {
    JobController.updateJobById(req.params.id, req.body, req.token_data)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
});

router.delete("/:id", SecureAPI(), (req, res, next) => {
    JobController.deleteJob(req.params.id)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
});

module.exports = router;

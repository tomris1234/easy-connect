const express = require("express");
const router = express.Router();
const RatingController = require('../controllers/rating.controller');
const { SecureAPI } = require("../utils/secure");

router.post('/', SecureAPI(), (req, res, next) => {
    RatingController.create(req.body, req.token_data)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
})

router.get('/all', SecureAPI(), (req, res, next) => {
    RatingController.getAll()
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
})

router.get('/:id', SecureAPI(), (req, res, next) => {
    RatingController.getOne(req.params.id)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
})

router.get('/average_rating/:user_id', SecureAPI(), (req, res, next) => {
    RatingController.getAverageRatingByUserId(req.params.user_id)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
})

router.put('/', SecureAPI(), (req, res, next) => {
    RatingController.update(req.body, req.token_data)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
})

router.delete('/:id', SecureAPI(), (req, res, next) => {
    RatingController.remove(req.params.id)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
})

module.exports = router;

const express = require("express");
const router = express.Router();
const VerificationController = require("../controllers/verification.controller");
const { SecureAPI } = require("../utils/secure");
var multer = require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname)
    }
})
var upload = multer({ storage: storage })

// const cloudinary = require("cloudinary").v2;
// cloudinary.config({
//     cloud_name: process.env.EASY_CLOUD_NAME,
//     api_key: process.env.EASY_CLOUD_API,
//     api_secret: process.env.EASY_CLOUD_SECRET
// });

// router.post("/", SecureAPI(), (req, res, next) => {
//     VerificationController.create(req.body, req.token_data)
//         .then(d => res.status(d.status).json(d))
//         .catch(e => next(e));
// });

// router.post("/cloud-upload", SecureAPI(), (req, res, next) => {
//     let _image = "public/images/3.jpg";
//     cloudinary.uploader
//         .upload(_image)
//         .then(result => {
//             res.status(200).send({
//                 message: "Image uploaded to cloud",
//                 result
//             });
//         })
//         .catch(error => {
//             res.status(500).send({
//                 message: "failure",
//                 error
//             });
//         });
// });

router.post("/", SecureAPI(), upload.single('fileUrl'), (req, res, next) => {
    VerificationController.create(req.body, req.token_data, req.file)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
});

// router.put("/", SecureAPI(), upload.single('fileUrl'), (req, res, next) => {
//     VerificationController.create(req.body, req.token_data, req.file)
//         .then(d => res.status(d.status).json(d))
//         .catch(e => next(e));
// });

router.post("/veriy", SecureAPI(), (req, res, next) => {
    VerificationController.veriy(req.body, req.token_data)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
});

router.put("/", SecureAPI(), upload.single('fileUrl'), (req, res, next) => {
    VerificationController.update(req.body, req.token_data, req.file)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
});

router.get("/student", SecureAPI(), (req, res, next) => {
    VerificationController.getAllforstudent()
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
});

router.get("/guest", SecureAPI(), (req, res, next) => {
    VerificationController.getAllforguest()
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
});

router.get("/:id", SecureAPI(), (req, res, next) => {
    VerificationController.getOne(req.params.id)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
});


router.get("/oneByUser/:id", SecureAPI(), (req, res, next) => {
    VerificationController.getByOneUser(req.params.id)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
});




router.delete("/:id", SecureAPI(), (req, res, next) => {
    VerificationController.deleteDocument(req.token_data, req.params.id)
        .then(d => res.status(d.status).json(d))
        .catch(e => next(e));
});

module.exports = router;

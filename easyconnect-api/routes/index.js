const router = require("express").Router();

const JobsRouter = require("./jobs");
const UserRouter = require("./users");
const VerificationRouter = require("./verification");
const TasksRouter = require("./tasks");
const RatingRouter = require("./rating");

router.get("/", (req, res, next) => {
    res.send({ success: true, message: "Welcome to IBM Challenge APP!" });
});

router.use("/api/v1/jobs", JobsRouter);
router.use("/api/v1/users", UserRouter);
router.use("/api/v1/verifications", VerificationRouter);
router.use("/api/v1/task", TasksRouter);
router.use("/api/v1/rating", RatingRouter);

module.exports = router;

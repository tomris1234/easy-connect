const express = require("express");
const router = express.Router();
const UserController = require("../controllers/user.controller");
var multer = require('multer')
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname)
  }
})
var upload = multer({ storage: storage })
const { SecureAPI } = require("../utils/secure");

router.get("/", (req, res, next) => {
  const page = req.query.page ? parseInt(req.query.page) : 1;
  const limit = req.query.limit ? parseInt(req.query.limit) : 10;
  const search = req.query.name ? req.query.name : null;
  UserController.listAll({ page, limit, search })
    .then((d) => res.json(d))
    .catch((e) => next(e));
});

router.post("/", (req, res, next) => {
  UserController.signup(req.body)
    .then((d) => res.json(d))
    .catch((e) => next(e));
});

router.post("/login", (req, res, next) => {
  UserController.login(req.body)
    .then((d) => res.json(d))
    .catch((e) => next(e));
});

router.get("/my-profile", SecureAPI(), (req, res, next) => {
  UserController.get(req.token_data._id)
    .then((d) => res.json(d))
    .catch((e) => next(e));
});

router.post("/forgot-password", (req, res, next) => {
  UserController.forgotPassword(req.body.username)
    .then((d) => res.json(d))
    .catch((e) => next(e));
});

router.post("/reset-password", (req, res, next) => {
  UserController.recoverPassword(req.body)
    .then((d) => res.json(d))
    .catch((e) => next(e));
});

router.get("/profile/:id", (req, res, next) => {
  UserController.get(req.params.id)
    .then((d) => res.json(d))
    .catch((e) => next(e));
});

router.post("/profile", SecureAPI(), (req, res, next) => {
  UserController.update(req.token_data._id, req.body)
    .then((d) => res.json(d))
    .catch((e) => next(e));
});

// router.post("/veriy_user", SecureAPI(), (req, res, next) => {
//   UserController.verify(req.body,req.token_data)
//     .then((d) => res.json(d))
//     .catch((e) => next(e));
// });

router.post("/change-password", SecureAPI(), (req, res, next) => {
  UserController.changePassword(req.token_data._id, req.body)
    .then((d) => res.json(d))
    .catch((e) => next(e));
});

router.delete("/remove/:id", SecureAPI(), (req, res, next) => {
  UserController.remove(req.params.id)
    .then((d) => res.json(d))
    .catch((e) => next(e));
});

router.get("/list/disable", SecureAPI(), (req, res, next) => {
  UserController.listDisablePeople()
    .then((d) => res.json(d))
    .catch((e) => next(e));
});

router.get("/list/students", SecureAPI(), (req, res, next) => {
  UserController.listStudents()
    .then((d) => res.json(d))
    .catch((e) => next(e));
});


router.get("/list/allnotverified", SecureAPI(), (req, res, next) => {
  UserController.listAllNotVerified()
    .then((d) => res.json(d))
    .catch((e) => next(e));
});



router.put("/update_profile_picture", SecureAPI(), upload.single('profilePic'), (req, res, next) => {
  UserController.updateProfilePic(req.file, req.token_data)
    .then((d) => res.json(d))
    .catch((e) => next(e));
});

router.post("/admin/login", (req, res, next) => {
  UserController.adminlogin(req.body)
    .then((d) => res.json(d))
    .catch((e) => next(e));
});

module.exports = router;

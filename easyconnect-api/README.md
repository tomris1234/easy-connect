# ibm-challenge

1. cd (change directory) project_directory.
2. Add variables.env file.
3. Create/Paste config snippets inside variables.env file or add PORT,DB_URL,APP_SECRET,SALT_ROUNDS,GMAIL_USER,GMAIL_PASS(for eg: PORT=7777, DB_URL=mongodb://localhost:27017/ibm-challenge, APP_SECRET=this is secret ,SALT_ROUNDS=5; ,GMAIL_USER=xyz@gmail.com, GMAIL_PASS=password12345 )
4. npm install
5. npm start

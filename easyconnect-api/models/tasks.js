// Accepted job becomes task.
const mongoose = require("mongoose");
var ObjectId = mongoose.Schema.ObjectId;
const TaskSchema = mongoose.Schema(
  {
    job: { type: ObjectId, required: true, ref: "Jobs" },
    status: {
      type: String,
      enum: ["Started", "Cancelled", "Completed"],
      default: "Started",
    },
    amount: { type: Number, required: true },
    student: { type: ObjectId, ref: "Users" },
    guest: { type: ObjectId, ref: "Users", required: true },
    specialNote: String,
    lastUpdatedBy: { type: ObjectId, ref: "Users" },
    lastUpdatedAt: Date,
  },
  {
    collection: "tasks",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
    toObject: { virtuals: true },
    toJSON: { virtuals: true },
  }
);

module.exports = mongoose.model("Tasks", TaskSchema);

const mongoose = require("mongoose");
const { ObjetId } = mongoose.Schema;

const UserSchema = mongoose.Schema(
  {
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    summary: [
      { type: String }
    ],
    phone: { type: String },
    profilePic: String,
    role: {
      type: String,
      enum: ["Guest", "Student", "Admin"],
      default: "Student",
    },
    isActive: { type: Boolean, default: true },
    isVerified:  {
      type: String,
      enum: ["Pending", "Verified", "Rejected"],
      default: "Pending",
    },
    extras: Object,
    token: {
      purpose: String,
      data: String,
      expiry_date: Date,
    },
  },
  {
    collection: "users",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
    toObject: { virtuals: true },
    toJSON: { virtuals: true },
  }
);

module.exports = mongoose.model("Users", UserSchema);

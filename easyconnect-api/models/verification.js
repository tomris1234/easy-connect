const mongoose = require("mongoose");
var ObjectId = mongoose.Schema.ObjectId;
const VerificationSchema = mongoose.Schema(
  {
    documentType: {
      type: String,
      enum: ["Student", "Guest"],
      required: true,
    },
    document: {
      fileUrl: String,
      Id: String,
    },
    status: {
      type: String,
      enum: ["Pending", "Verified", "Rejected"],
      default: "Pending",
    },
    note: String,
    requestedBy: { type: ObjectId, ref: "Users" },
    lastUpdatedBy: { type: ObjectId, ref: "Users" },
  },
  {
    collection: "verifications",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
    toObject: { virtuals: true },
    toJSON: { virtuals: true },
  }
);

module.exports = mongoose.model("Verifications", VerificationSchema);

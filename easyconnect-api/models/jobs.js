const mongoose = require("mongoose");
var ObjectId = mongoose.Schema.ObjectId;
const JobSchema = mongoose.Schema(
    {
        title: { type: String, required: true },
        description: { type: String, required: true },
        requirements: [{ type: String }],
        minPrice: { type: Number },
        maxPrice: { type: Number },
        location: { type: String, required: true },
        expiryDate: { type: Date },
        bidders: [
            { type: ObjectId, ref: "Users", note: String, bidAmount: Number }
        ],
        status: {
            type: String,
            enum: ["Open", "Locked", "Rejected", "Completed"],
            default: "Open"
        },
        createdBy: { type: ObjectId, ref: "Users" },
        updatedBy: { type: ObjectId, ref: "Users" },
        doingBy: { type: ObjectId, ref: "Users" }
    },
    {
        collection: "jobs",
        timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
        toObject: { virtuals: true },
        toJSON: { virtuals: true }
    }
);

module.exports = mongoose.model("Jobs", JobSchema);

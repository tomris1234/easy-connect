// Accepted job becomes task.
const mongoose = require("mongoose");
var ObjectId = mongoose.Schema.ObjectId;
const RatingSchema = mongoose.Schema(
    {
        user: { type: ObjectId, required: true, ref: "Users" },
        rating: {
            type: Number
        },
        note: { type: String },
        ratingBy: { type: ObjectId, ref: "Users" }
    },
    {
        collection: "rating",
        timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
        toObject: { virtuals: true },
        toJSON: { virtuals: true },
    }
);

module.exports = mongoose.model("rating", RatingSchema);
